(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[10],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/contact.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/contact.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-loading-overlay */ "./node_modules/vue-loading-overlay/dist/vue-loading.min.js");
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-loading-overlay/dist/vue-loading.css */ "./node_modules/vue-loading-overlay/dist/vue-loading.css");
/* harmony import */ var vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.js */ "./node_modules/sweetalert2/dist/sweetalert2.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2/src/sweetalert2.scss */ "./node_modules/sweetalert2/src/sweetalert2.scss");
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_3__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // Import stylesheet




/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      comment: {
        name: '',
        email: '',
        body: ''
      },
      loading: true,
      error: ''
    };
  },
  created: function created() {
    var _this = this;

    setTimeout(function () {
      _this.loading = false;
    }, 2000);
  },
  methods: {
    Comment: function Comment() {
      var _this2 = this;

      this.$validator.validateAll().then(function (result) {
        if (!result) {
          _this2.error = "Molimo Vas unesite ispravne podatke!";
          return;
        }

        var uri = '/api/contact/create';
        var name = _this2.comment.name;
        var email = _this2.comment.email;
        var body = _this2.comment.body;

        _this2.axios.post(uri, {
          name: name,
          email: email,
          body: body
        }, {
          headers: {
            'Access-Control-Allow-Origin': '*'
          }
        }).then(function (response) {
          _this2.clearForm();

          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
            position: 'center',
            icon: 'success',
            title: 'Komentar je uspešno poslat! Bice objavljen nakon što ga pregleda administrator.',
            showConfirmButton: false,
            timer: 1500
          }); //	this.Post();
        });
      });
    },
    clearForm: function clearForm() {
      this.comment.name = null;
      this.comment.email = null;
      this.comment.body = null;
      this.error = null;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/contact.vue?vue&type=template&id=1f6534ff&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/contact.vue?vue&type=template&id=1f6534ff& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { attrs: { id: "rout" } }, [
    _vm.loading ? _c("div") : _vm._e(),
    _vm._v(" "),
    _vm._m(0),
    _vm._v(" "),
    _c("div", [
      _c("section", { staticClass: "contact-section spad" }, [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _vm._m(1),
            _vm._v(" "),
            _c("div", { staticClass: "col-lg-6 offset-lg-1" }, [
              _c("div", { staticClass: "contact-form" }, [
                _vm.error
                  ? _c("div", { staticClass: "alert-danger r text-center" }, [
                      _c("h3", [_vm._v(_vm._s(_vm.error))])
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _c("div", { staticClass: "leave-comment" }, [
                  _c("h4", [_vm._v("Leave A Comment")]),
                  _vm._v(" "),
                  _c("p", [
                    _vm._v(
                      "Our staff will call back later and answer your questions."
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "form",
                    {
                      staticClass: "comment-form",
                      attrs: { novalidate: "" },
                      on: {
                        submit: function($event) {
                          $event.preventDefault()
                          return _vm.Comment($event)
                        }
                      }
                    },
                    [
                      _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "col-lg-6" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required",
                                expression: "'required'"
                              },
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.comment.name,
                                expression: "comment.name"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              name: "name",
                              type: "text",
                              placeholder: "Your Name"
                            },
                            domProps: { value: _vm.comment.name },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.comment,
                                  "name",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-lg-6" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.comment.email,
                                expression: "comment.email"
                              },
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "email",
                                expression: "'email'"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              name: "email",
                              placeholder: "Your Email",
                              type: "text"
                            },
                            domProps: { value: _vm.comment.email },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.comment,
                                  "email",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.errors.has("email"),
                                  expression: "errors.has('email')"
                                }
                              ],
                              staticClass: "help-block alert alert-danger"
                            },
                            [
                              _vm._v(
                                "\n\t\t\t\t\t\t\t\t\t\t\t\t" +
                                  _vm._s(_vm.errors.first("email")) +
                                  "\n\t\t\t\t\t\t\t\t\t\t\t"
                              )
                            ]
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-lg-12" }, [
                          _c("textarea", {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required",
                                expression: "'required'"
                              },
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.comment.body,
                                expression: "comment.body"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              name: "comment",
                              placeholder: "Your message"
                            },
                            domProps: { value: _vm.comment.body },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.comment,
                                  "body",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "button",
                            {
                              staticClass: "site-btn",
                              attrs: { type: "submit" }
                            },
                            [_vm._v("Send message")]
                          )
                        ])
                      ])
                    ]
                  )
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "map spad" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "map-inner" }, [
          _c("div", { staticClass: "mapouter" }, [
            _c("div", { staticClass: "gmap_canvas" }, [
              _c("iframe", {
                attrs: {
                  width: "1080",
                  height: "500",
                  id: "gmap_canvas",
                  src:
                    "https://maps.google.com/maps?q=Bra%C4%87e%20Jerkovi%C4%87%20100&t=&z=13&ie=UTF8&iwloc=&output=embed",
                  frameborder: "0",
                  scrolling: "no",
                  marginheight: "0",
                  marginwidth: "0"
                }
              }),
              _vm._v(" "),
              _c("a", { attrs: { href: "https://www.home.mdi.in.rs" } }, [
                _vm._v("MDI")
              ])
            ])
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-lg-5" }, [
      _c("div", { staticClass: "contact-title" }, [
        _c("h4", [_vm._v("Contacts Us")]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "Contrary to popular belief, Lorem Ipsum is simply random text. It has roots in a piece of\n                            classical Latin literature from 45 BC, maki years old."
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "contact-widget" }, [
        _c("div", { staticClass: "cw-item" }, [
          _c("div", { staticClass: "ci-icon" }, [
            _c("i", { staticClass: "fa fa-map-marker" })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "ci-text" }, [
            _c("span", [_vm._v("Address:")]),
            _vm._v(" "),
            _c("p", [_vm._v("Braće Jerković 100/15, Beograd")])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "cw-item" }, [
          _c("div", { staticClass: "ci-icon" }, [
            _c("i", { staticClass: "fa fa-mobile" })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "ci-text" }, [
            _c("span", [_vm._v("Phone:")]),
            _vm._v(" "),
            _c("p", [_vm._v("+381691567555")])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "cw-item" }, [
          _c("div", { staticClass: "ci-icon" }, [
            _c("i", { staticClass: "far fa-envelope fa-1x" })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "ci-text" }, [
            _c("span", [_vm._v("Email:")]),
            _vm._v(" "),
            _c("p", [_vm._v("veljkoveljkovic.mdi@gmail.com")])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/front/contact.vue":
/*!***************************************************!*\
  !*** ./resources/js/components/front/contact.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _contact_vue_vue_type_template_id_1f6534ff___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./contact.vue?vue&type=template&id=1f6534ff& */ "./resources/js/components/front/contact.vue?vue&type=template&id=1f6534ff&");
/* harmony import */ var _contact_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./contact.vue?vue&type=script&lang=js& */ "./resources/js/components/front/contact.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _contact_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _contact_vue_vue_type_template_id_1f6534ff___WEBPACK_IMPORTED_MODULE_0__["render"],
  _contact_vue_vue_type_template_id_1f6534ff___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/front/contact.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/front/contact.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/front/contact.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_contact_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./contact.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/contact.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_contact_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/front/contact.vue?vue&type=template&id=1f6534ff&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/front/contact.vue?vue&type=template&id=1f6534ff& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_contact_vue_vue_type_template_id_1f6534ff___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./contact.vue?vue&type=template&id=1f6534ff& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/contact.vue?vue&type=template&id=1f6534ff&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_contact_vue_vue_type_template_id_1f6534ff___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_contact_vue_vue_type_template_id_1f6534ff___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);