(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[1],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/Ecommerce.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/Ecommerce.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-loading-overlay */ "./node_modules/vue-loading-overlay/dist/vue-loading.min.js");
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.js */ "./node_modules/sweetalert2/dist/sweetalert2.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2/src/sweetalert2.scss */ "./node_modules/sweetalert2/src/sweetalert2.scss");
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-loading-overlay/dist/vue-loading.css */ "./node_modules/vue-loading-overlay/dist/vue-loading.css");
/* harmony import */ var vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay_dist_vue_loading_css__WEBPACK_IMPORTED_MODULE_4__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



 // Import stylesheet



var filteredProducts = _toConsumableArray(Array(150).keys()).map(function (i) {
  return {
    id: i + 1,
    name: 'Item ' + (i + 1)
  };
});

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {},
  data: function data() {
    return _defineProperty({
      likes: {},
      pretraga: '',
      pager: {},
      isLoading: false,
      fullPage: true,
      isActive: false,
      image_size: {
        width: 200,
        height: 200,
        "class": 'm1'
      },
      image_src: '/uploads/products/',
      category_id: '',
      products: [],
      categories: [],
      search: '',
      boja: '',
      velicina: '',
      product: {
        id: '',
        name: '',
        description: '',
        categor_id: '',
        supplier_id: '',
        units: '',
        price: '',
        discount_price: '',
        image: ''
      },

      /*	category: {
      		id: '',
      		kategorija: '',
      		
      	}, */
      brands: [],
      brand: {
        id: '',
        brand: ''
      },
      tags: {
        id: '',
        tag: ''
      },
      pageOfItems: [],
      product_id: '',
      find: '',
      kategorija: '',
      brend: [],
      min_price: '',
      max_price: '',
      size: ''
    }, "boja", '');
  },
  created: function created() {
    if (this.$route.name == 'category') {
      this.category(this.$route.params.id);
      this.search = '';
    }

    if (this.$route.name == 'tag') {
      this.tagovi(this.$route.params.id);
      this.search = '';
    } else if (this.$route.path === '/') {
      this.start();
    } //  this.allCategory();
    // this.clearForm();

  },
  computed: _objectSpread({
    filteredProducts: function filteredProducts() {
      var _this = this;

      if (this.search) {
        this.notfilteredproducts();
      }

      return this.allProducts.filter(function (product) {
        return product.name.toLowerCase().match(_this.search.toLowerCase());
      });
    }
  }, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])(['allCategories', 'allTags', 'allBrands', 'allProducts', 'callMessage'])),
  mounted: function mounted() {// Set the initial number of items
  },
  components: {
    Loading: vue_loading_overlay__WEBPACK_IMPORTED_MODULE_1___default.a
  },
  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])(["start", "tagovi", "category", "notfilteredproducts"]), {
    like: function like(product) {
      var _this2 = this;

      this.$store.dispatch('addLike', product).then(function (response) {
        setTimeout(function () {
          _this2.onsuccess();
        }, 1100);
      });
    },
    resetfilter: function resetfilter() {
      this.min_price = '', this.max_price = '', this.size = '', this.brend = [], this.kategorija = '';
      this.boja = '';
      this.start();
    },
    cart: function cart(product) {
      var _this3 = this;

      this.$store.dispatch('addCart', product).then(function (response) {
        setTimeout(function () {
          _this3.onsuccess();
        }, 1100);
      });
    },
    onsuccess: function onsuccess() {
      sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
        position: 'top-end',
        icon: 'warning',
        title: this.callMessage,
        showConfirmButton: false,
        timer: 1000
      });
    },
    AdvanceSearch: function AdvanceSearch() {
      var _this4 = this;

      var min_price = this.min_price;
      var max_price = this.max_price;
      var size = this.size;
      var brend = this.brend[0];
      var category = this.kategorija;
      var color = this.boja;
      var uri = '/api/filter';
      axios.post(uri, {
        brend: brend,
        category: category,
        min_price: min_price,
        max_price: max_price,
        size: size,
        color: color
      }, {
        headers: {
          'Access-Control-Allow-Origin': '*'
        }
      }).then(function (response) {
        _this4.search = '';
        _this4.advancedfilter = true;
        var products = response.data.product;

        _this4.$store.dispatch('filter', products);
      });
    },

    /*	start() {
    			let uri = '/api';
    	
    	axios.get(uri,  {
    				headers: {
    					'Access-Control-Allow-Origin': '*',
    				}
    			}).then(response => {
    		this.products = response.data.product
    		this.categories = response.data.cat
    		this.brands = response.data.brands
    		this.tags = response.data.tags
    		this.basicProducts = this.products
    		
    				
    				
    	})
    	}, 
    	tagovi() {
    	
    		let uri = '/api/tag/'+this.$route.params.id;
    	this.axios.get(uri,  {
    				headers: {
    					'Access-Control-Allow-Origin': '*',
    				}
    			}).then(response => {
    				this.isLoading = false
    				this.products = response.data.product
    				this.categories = response.data.cat
    				this.brands = response.data.brands
    				this.tags = response.data.tags
    				this.basicProducts = this.products
    				
    				 });	
    				
      
       
    	}, */
    onChangePage: function onChangePage(pageOfItems) {
      // update page of items
      this.pageOfItems = pageOfItems;
    },
    fieldChange: function fieldChange(e) {
      var _this5 = this;

      var selectedFile = new FileReader();
      selectedFile.readAsDataURL(e.target.files[0]);

      selectedFile.onload = function (e) {
        _this5.product.image = e.target.result;
        _this5.image_update = true;
      };

      console.log(this.product.image);
    },
    onCancel: function onCancel() {
      console.log('User cancelled the loader.');
    }
  })
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/Ecommerce.vue?vue&type=style&index=0&id=24ac713f&scoped=true&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/Ecommerce.vue?vue&type=style&index=0&id=24ac713f&scoped=true&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\ni[data-v-24ac713f] { cursor: pointer;}\ninput[type=\"checkbox\"][data-v-24ac713f]:focus {\n   outline: none !important;\n\tborder-color: #ED502E !important;\n\tbox-shadow: 0 0 5px #ED502E !important;\n}\ninput[type=\"radio\"]:checked+label[data-v-24ac713f]{  color: #E7AB3C !important;}\n.na[data-v-24ac713f] {\n\t\tbackground-color: #FC611F !important;\n\t\theight: auto;\n\t\tpadding-bottom: 10px;\n\t\tpadding-top: 10px;\n}\n.naa[data-v-24ac713f] {\n\t\tbackground-color: lightgray;\n\t\theight: auto;\n\t\tpadding-bottom: 10px;\n\t\tpadding-top: 10px;\n}\n.nav-link[data-v-24ac713f]:hover {\n\t\tcolor:black !important;\n}\n.box[data-v-24ac713f]{\n\t\t position: relative;\n}\n.box[data-v-24ac713f] {\n\tposition: relative;\n}\n.na[data-v-24ac713f] {\n\tbackground-color: #FC611F !important;\n}\n.sticky[data-v-24ac713f] {\n\tlist-style-type: none;\n\tposition: -webkit-sticky;\n\tposition: sticky;\n\toverflow: hidden;\n\ttop: 82px;\n\twidth: 100%;\n\tz-index: 9999;\n}\n.s[data-v-24ac713f] {\n\t\ttext-decoration: none !important;\n}\n.small-text[data-v-24ac713f] {\n\t\tfont-size: 14px;\n}\ninput[type=checkbox][data-v-24ac713f]:checked:after {\n    background-color: #E7AB3C;\n}\na[data-v-24ac713f]:hover {background-color: #E7AB3C;}\n.product-box[data-v-24ac713f] {\n\t\tborder: 1px solid #cccccc;\n\t\tpadding: 10px 15px;\n}\n.hero-section[data-v-24ac713f] {\n\t\theight: 30vh;\n\t\tbackground: #ababab;\n\t\talign-items: center;\n\t\tmargin-bottom: 20px;\n\t\tmargin-top: -20px;\n}\n.title[data-v-24ac713f] {\n\t\tfont-size: 60px;\n\t\tcolor: #ffffff;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/Ecommerce.vue?vue&type=style&index=0&id=24ac713f&scoped=true&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/Ecommerce.vue?vue&type=style&index=0&id=24ac713f&scoped=true&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Ecommerce.vue?vue&type=style&index=0&id=24ac713f&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/Ecommerce.vue?vue&type=style&index=0&id=24ac713f&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/Ecommerce.vue?vue&type=template&id=24ac713f&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/Ecommerce.vue?vue&type=template&id=24ac713f&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { attrs: { id: "rout" } }, [
    _c("div", { staticClass: "inner-header" }, [
      _c("div", { staticClass: "offset-lg-3 col-lg-6 col-md-6 offset-lg-3" }, [
        _c("div", { staticClass: "advanced-search" }, [
          _c(
            "button",
            { staticClass: "category-btn", attrs: { type: "button" } },
            [_vm._v("All Categories")]
          ),
          _vm._v(" "),
          _c("form", { staticClass: "input-group", attrs: { action: "" } }, [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.find,
                  expression: "find"
                }
              ],
              attrs: { type: "search", placeholder: "What do you need?" },
              domProps: { value: _vm.find },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.find = $event.target.value
                }
              }
            }),
            _vm._v(" "),
            _c(
              "button",
              {
                attrs: { type: "button" },
                on: {
                  click: function($event) {
                    _vm.search = _vm.find
                  }
                }
              },
              [_c("i", { staticClass: "fa fa-search fa-1x" })]
            )
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      [
        _c("loading", {
          attrs: {
            active: _vm.isLoading,
            "can-cancel": true,
            "on-cancel": _vm.onCancel,
            "is-full-page": _vm.fullPage
          },
          on: {
            "update:active": function($event) {
              _vm.isLoading = $event
            }
          }
        }),
        _vm._v(" "),
        _c("section", { staticClass: "product-shop spad" }, [
          _c("div", { staticClass: "container" }, [
            _c("div", { staticClass: "row" }, [
              _c(
                "div",
                {
                  staticClass:
                    "col-lg-3 col-md-6 col-sm-8 order-2 order-lg-1 produts-sidebar-filter"
                },
                [
                  _c("br"),
                  _c("br"),
                  _vm._v(" "),
                  _c(
                    "form",
                    {
                      attrs: { novalidate: "" },
                      on: {
                        submit: function($event) {
                          $event.preventDefault()
                          return _vm.AdvanceSearch()
                        }
                      }
                    },
                    [
                      _c(
                        "div",
                        { staticClass: "filter-widget" },
                        [
                          _c("h4", { staticClass: "fw-title" }, [
                            _vm._v("Categories")
                          ]),
                          _vm._v(" "),
                          _vm._l(_vm.allCategories, function(cat) {
                            return _c(
                              "div",
                              {
                                model: {
                                  value: _vm.kategorija,
                                  callback: function($$v) {
                                    _vm.kategorija = $$v
                                  },
                                  expression: "kategorija"
                                }
                              },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.kategorija,
                                      expression: "kategorija"
                                    }
                                  ],
                                  attrs: {
                                    type: "radio",
                                    id: cat.kategorija,
                                    name: "kategorija",
                                    hidden: ""
                                  },
                                  domProps: {
                                    value: cat.id,
                                    checked: _vm._q(_vm.kategorija, cat.id)
                                  },
                                  on: {
                                    change: function($event) {
                                      _vm.kategorija = cat.id
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "label",
                                  { attrs: { for: cat.kategorija } },
                                  [_vm._v(_vm._s(cat.kategorija))]
                                ),
                                _c("br")
                              ]
                            )
                          })
                        ],
                        2
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "filter-widget" }, [
                        _c("h4", { staticClass: "fw-title" }, [
                          _vm._v("Brand")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "fw-brand-check" }, [
                          _c(
                            "div",
                            { staticClass: "bc-item " },
                            [
                              _c("b-form-checkbox-group", {
                                attrs: {
                                  stacked: "",
                                  options: _vm.allBrands,
                                  type: "checkbox",
                                  "value-field": "id",
                                  "text-field": "brand",
                                  name: "brend",
                                  "disabled-field": "notEnabled"
                                },
                                model: {
                                  value: _vm.brend,
                                  callback: function($$v) {
                                    _vm.brend = $$v
                                  },
                                  expression: "brend"
                                }
                              })
                            ],
                            1
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "filter-widget" }, [
                        _c("h4", { staticClass: "fw-title" }, [
                          _vm._v("Price")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "filter-range-wrap" }, [
                          _c("div", { staticClass: "range-slider" }, [
                            _c("div", { staticClass: "price-input" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.min_price,
                                    expression: "min_price"
                                  }
                                ],
                                attrs: {
                                  type: "text",
                                  id: "minamount",
                                  name: "min_price"
                                },
                                domProps: { value: _vm.min_price },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.min_price = $event.target.value
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.max_price,
                                    expression: "max_price"
                                  }
                                ],
                                attrs: {
                                  type: "text",
                                  id: "maxamount",
                                  name: "max_price"
                                },
                                domProps: { value: _vm.max_price },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.max_price = $event.target.value
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _vm._m(0)
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "filter-widget" }, [
                        _c("div", { staticClass: "fw-color-choose" }, [
                          _c("div", { staticClass: "cs-item" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.boja,
                                  expression: "boja"
                                }
                              ],
                              attrs: {
                                type: "radio",
                                id: "cs-black",
                                value: "Black"
                              },
                              domProps: { checked: _vm._q(_vm.boja, "Black") },
                              on: {
                                change: function($event) {
                                  _vm.boja = "Black"
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c(
                              "label",
                              {
                                staticClass: "cs-black",
                                attrs: { for: "cs-black" }
                              },
                              [_vm._v("Black")]
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "cs-item" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.boja,
                                  expression: "boja"
                                }
                              ],
                              attrs: {
                                type: "radio",
                                id: "cs-violet",
                                value: "Violet"
                              },
                              domProps: { checked: _vm._q(_vm.boja, "Violet") },
                              on: {
                                change: function($event) {
                                  _vm.boja = "Violet"
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c(
                              "label",
                              {
                                staticClass: "cs-violet",
                                attrs: { for: "cs-violet" }
                              },
                              [_vm._v("Violet")]
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "cs-item" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.boja,
                                  expression: "boja"
                                }
                              ],
                              attrs: {
                                type: "radio",
                                id: "cs-blue",
                                value: "Blue"
                              },
                              domProps: { checked: _vm._q(_vm.boja, "Blue") },
                              on: {
                                change: function($event) {
                                  _vm.boja = "Blue"
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c(
                              "label",
                              {
                                staticClass: "cs-blue",
                                attrs: { for: "cs-blue" }
                              },
                              [_vm._v("Blue")]
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "cs-item" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.boja,
                                  expression: "boja"
                                }
                              ],
                              attrs: {
                                type: "radio",
                                id: "cs-yellow",
                                value: "Yellow"
                              },
                              domProps: { checked: _vm._q(_vm.boja, "Yellow") },
                              on: {
                                change: function($event) {
                                  _vm.boja = "Yellow"
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c(
                              "label",
                              {
                                staticClass: "cs-yellow",
                                attrs: { for: "cs-yellow" }
                              },
                              [_vm._v("Yellow")]
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "cs-item" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.boja,
                                  expression: "boja"
                                }
                              ],
                              attrs: {
                                type: "radio",
                                id: "cs-red",
                                value: "Red"
                              },
                              domProps: { checked: _vm._q(_vm.boja, "Red") },
                              on: {
                                change: function($event) {
                                  _vm.boja = "Red"
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c(
                              "label",
                              {
                                staticClass: "cs-red",
                                attrs: { for: "cs-red" }
                              },
                              [_vm._v("Red")]
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "cs-item" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.boja,
                                  expression: "boja"
                                }
                              ],
                              attrs: {
                                type: "radio",
                                id: "cs-green",
                                value: "Green"
                              },
                              domProps: { checked: _vm._q(_vm.boja, "Green") },
                              on: {
                                change: function($event) {
                                  _vm.boja = "Green"
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c(
                              "label",
                              {
                                staticClass: "cs-green",
                                attrs: { for: "cs-green" }
                              },
                              [_vm._v("Green")]
                            )
                          ])
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "filter-widget" }, [
                        _c("h4", { staticClass: "fw-title" }, [_vm._v("Size")]),
                        _vm._v(" "),
                        _c("div", { staticClass: "fw-size-choose" }, [
                          _c("div", { staticClass: "sc-item" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.size,
                                  expression: "size"
                                }
                              ],
                              attrs: {
                                type: "radio",
                                id: "s-size",
                                value: "S"
                              },
                              domProps: { checked: _vm._q(_vm.size, "S") },
                              on: {
                                change: function($event) {
                                  _vm.size = "S"
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("label", { attrs: { for: "s-size" } }, [
                              _vm._v("s")
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "sc-item" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.size,
                                  expression: "size"
                                }
                              ],
                              attrs: {
                                type: "radio",
                                id: "m-size",
                                value: "M"
                              },
                              domProps: { checked: _vm._q(_vm.size, "M") },
                              on: {
                                change: function($event) {
                                  _vm.size = "M"
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("label", { attrs: { for: "m-size" } }, [
                              _vm._v("m")
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "sc-item" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.size,
                                  expression: "size"
                                }
                              ],
                              attrs: {
                                type: "radio",
                                id: "l-size",
                                value: "L"
                              },
                              domProps: { checked: _vm._q(_vm.size, "L") },
                              on: {
                                change: function($event) {
                                  _vm.size = "L"
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("label", { attrs: { for: "l-size" } }, [
                              _vm._v("l")
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "sc-item" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.size,
                                  expression: "size"
                                }
                              ],
                              attrs: {
                                type: "radio",
                                id: "xs-size",
                                value: "XS"
                              },
                              domProps: { checked: _vm._q(_vm.size, "XS") },
                              on: {
                                change: function($event) {
                                  _vm.size = "XS"
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("label", { attrs: { for: "xs-size" } }, [
                              _vm._v("xs")
                            ])
                          ])
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "filter-widget" }, [
                        _c(
                          "button",
                          {
                            staticClass: "filter-btn",
                            attrs: { type: "submit" }
                          },
                          [_vm._v("Filter")]
                        ),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            staticClass: "filter-btn",
                            attrs: { type: "button" },
                            on: {
                              click: function($event) {
                                $event.preventDefault()
                                return _vm.resetfilter($event)
                              }
                            }
                          },
                          [_vm._v("Reset Filter")]
                        )
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "filter-widget" }, [
                    _c("h4", { staticClass: "fw-title" }, [_vm._v("Tags")]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "fw-tags" },
                      _vm._l(_vm.allTags, function(tag, index) {
                        return _c(
                          "a",
                          {
                            attrs: {
                              href: _vm.$router.resolve({
                                name: "tag",
                                params: { id: tag.id }
                              }).href
                            },
                            on: { key: index }
                          },
                          [_vm._v(_vm._s(tag.tag))]
                        )
                      }),
                      0
                    )
                  ])
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-9 order-1 order-lg-2" }, [
                _c("div", { staticClass: "product-list" }, [
                  _c(
                    "div",
                    { staticClass: "row" },
                    _vm._l(_vm.pageOfItems, function(product, index) {
                      return _c(
                        "div",
                        {
                          staticClass: "col-lg-4 col-sm-6",
                          on: { key: index }
                        },
                        [
                          _c("div", { staticClass: "product-item" }, [
                            _c(
                              "div",
                              { staticClass: "pi-pic" },
                              [
                                _c("img", {
                                  attrs: {
                                    src: _vm.image_src + product.image,
                                    alt: ""
                                  }
                                }),
                                _vm._v(" "),
                                _vm._l(_vm.allTags, function(tag, index) {
                                  return _c(
                                    "div",
                                    {
                                      staticClass: "sale pp-sale",
                                      on: { key: index }
                                    },
                                    [
                                      tag.id == product.tag_id
                                        ? _c("span", [_vm._v(_vm._s(tag.tag))])
                                        : _vm._e()
                                    ]
                                  )
                                }),
                                _vm._v(" "),
                                _c("div", { staticClass: "icon" }, [
                                  _c("i", {
                                    staticClass: "fa fa-heart-o",
                                    on: {
                                      click: function($event) {
                                        return _vm.like(product)
                                      }
                                    }
                                  })
                                ]),
                                _vm._v(" "),
                                _c("ul", [
                                  _c("li", { staticClass: "w-icon active" }, [
                                    _c("a", [
                                      _c("i", {
                                        staticClass: "fa fa-shopping-bag",
                                        on: {
                                          click: function($event) {
                                            $event.preventDefault()
                                            return _vm.cart(product)
                                          }
                                        }
                                      })
                                    ])
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "li",
                                    { staticClass: "quick-view" },
                                    [
                                      _c(
                                        "router-link",
                                        {
                                          attrs: {
                                            to: {
                                              name: "single-products",
                                              params: { id: product.id }
                                            }
                                          }
                                        },
                                        [_vm._v("+ Quick View")]
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _vm._m(1, true)
                                ])
                              ],
                              2
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "pi-text" },
                              [
                                _vm._l(_vm.allCategories, function(
                                  category,
                                  index
                                ) {
                                  return _c(
                                    "div",
                                    {
                                      staticClass: "catagory-name",
                                      on: { key: index }
                                    },
                                    [
                                      category.id == product.category_id
                                        ? _c("span", [
                                            _vm._v(_vm._s(category.kategorija))
                                          ])
                                        : _vm._e()
                                    ]
                                  )
                                }),
                                _vm._v(" "),
                                _c("h4", [
                                  _c("span", [_vm._v(_vm._s(product.name))])
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "product-price" }, [
                                  _vm._v(
                                    "\n\t\t\t\t\t\t\t\t\t\t\t\t\t   " +
                                      _vm._s(product.price) +
                                      "\n\t\t\t\t\t\t\t\t\t\t\t\t\t  "
                                  )
                                ])
                              ],
                              2
                            )
                          ])
                        ]
                      )
                    }),
                    0
                  )
                ])
              ])
            ])
          ])
        ])
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "card-footer pb-0 pt-3 d-flex justify-content-center" },
      [
        _c("jw-pagination", {
          attrs: { items: _vm.filteredProducts },
          on: { changePage: _vm.onChangePage }
        })
      ],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "price-range ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content",
        attrs: { "data-min": "33", "data-max": "98" }
      },
      [
        _c("div", {
          staticClass: "ui-slider-range ui-corner-all ui-widget-header"
        }),
        _vm._v(" "),
        _c("span", {
          staticClass: "ui-slider-handle ui-corner-all ui-state-default",
          attrs: { tabindex: "0" }
        }),
        _vm._v(" "),
        _c("span", {
          staticClass: "ui-slider-handle ui-corner-all ui-state-default",
          attrs: { tabindex: "0" }
        })
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "w-icon" }, [
      _c("a", { attrs: { href: "" } }, [
        _c("i", { staticClass: "fa fa-random" })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/front/Ecommerce.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/front/Ecommerce.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Ecommerce_vue_vue_type_template_id_24ac713f_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Ecommerce.vue?vue&type=template&id=24ac713f&scoped=true& */ "./resources/js/components/front/Ecommerce.vue?vue&type=template&id=24ac713f&scoped=true&");
/* harmony import */ var _Ecommerce_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Ecommerce.vue?vue&type=script&lang=js& */ "./resources/js/components/front/Ecommerce.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Ecommerce_vue_vue_type_style_index_0_id_24ac713f_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Ecommerce.vue?vue&type=style&index=0&id=24ac713f&scoped=true&lang=css& */ "./resources/js/components/front/Ecommerce.vue?vue&type=style&index=0&id=24ac713f&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Ecommerce_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Ecommerce_vue_vue_type_template_id_24ac713f_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Ecommerce_vue_vue_type_template_id_24ac713f_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "24ac713f",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/front/Ecommerce.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/front/Ecommerce.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/front/Ecommerce.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Ecommerce_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Ecommerce.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/Ecommerce.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Ecommerce_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/front/Ecommerce.vue?vue&type=style&index=0&id=24ac713f&scoped=true&lang=css&":
/*!**************************************************************************************************************!*\
  !*** ./resources/js/components/front/Ecommerce.vue?vue&type=style&index=0&id=24ac713f&scoped=true&lang=css& ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Ecommerce_vue_vue_type_style_index_0_id_24ac713f_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Ecommerce.vue?vue&type=style&index=0&id=24ac713f&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/Ecommerce.vue?vue&type=style&index=0&id=24ac713f&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Ecommerce_vue_vue_type_style_index_0_id_24ac713f_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Ecommerce_vue_vue_type_style_index_0_id_24ac713f_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Ecommerce_vue_vue_type_style_index_0_id_24ac713f_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Ecommerce_vue_vue_type_style_index_0_id_24ac713f_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Ecommerce_vue_vue_type_style_index_0_id_24ac713f_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/front/Ecommerce.vue?vue&type=template&id=24ac713f&scoped=true&":
/*!************************************************************************************************!*\
  !*** ./resources/js/components/front/Ecommerce.vue?vue&type=template&id=24ac713f&scoped=true& ***!
  \************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Ecommerce_vue_vue_type_template_id_24ac713f_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Ecommerce.vue?vue&type=template&id=24ac713f&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/Ecommerce.vue?vue&type=template&id=24ac713f&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Ecommerce_vue_vue_type_template_id_24ac713f_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Ecommerce_vue_vue_type_template_id_24ac713f_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);