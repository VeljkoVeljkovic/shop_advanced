(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[5],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/SingleProduct.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/SingleProduct.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.js */ "./node_modules/sweetalert2/dist/sweetalert2.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2/src/sweetalert2.scss */ "./node_modules/sweetalert2/src/sweetalert2.scss");
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_2__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return _defineProperty({
      limit: 4,
      product: {
        id: '',
        quantity: '',
        name: '',
        description: '',
        categor_id: '',
        supplier_id: '',
        units: '',
        price: '',
        discount_price: '',
        image: ''
      },
      image_src: '/uploads/products/',
      product1: 'uploads/img/product-single/product-1.jpg',
      product2: 'uploads/img/product-single/product-2.jpg',
      product3: 'uploads/img/product-single/product-3.jpg',
      likes: {},
      pretraga: '',
      isActive: false,
      image_size: {
        width: 200,
        height: 200,
        "class": 'm1'
      },
      category_id: '',
      products: [],
      categories: [],
      search: '',
      boja: '',
      velicina: '',
      brands: [],
      brand: {
        id: '',
        brand: ''
      },
      tags: {
        id: '',
        tag: ''
      },
      product_id: '',
      find: '',
      kategorija: '',
      brend: [],
      min_price: '',
      max_price: '',
      size: ''
    }, "boja", '');
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])(['allCategories', 'allTags', 'allBrands', 'allProducts', 'callMessage', 'allCoupons', 'alreadyInCart']), {
    computedObj: function computedObj() {
      return this.limit ? this.allProducts.slice(0, this.limit) : this.allProducts;
    }
  }),
  beforeMount: function beforeMount() {
    var _this = this;

    var uri = "/api/products/".concat(this.$route.params.id);
    axios.get(uri, {
      headers: {
        'Access-Control-Allow-Origin': '*'
      }
    }).then(function (response) {
      _this.product = response.data;

      _this.check(_this.product);
    });
  },
  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])(["start", "tagovi", "category", "notfilteredproducts"]), {
    like: function like(product) {
      var _this2 = this;

      this.$store.dispatch('addLike', product).then(function (response) {
        return _this2.$store.getters.alreadyInCart;
      });
    },
    check: function check(product) {
      var _this3 = this;

      this.$store.dispatch('alreadyInCart', product).then(function (response) {
        _this3.inCart;
      });
    },
    quantity: function quantity(product) {
      var _this4 = this;

      this.$store.dispatch('changeQuantity', product).then(function (response) {
        _this4.swall();
      });
    },
    remove: function remove(product) {
      var _this5 = this;

      this.$store.dispatch('remove', product);
      this.swall();
      setTimeout(function () {
        _this5.check(_this5.product);
      }, 1200);
    },
    cart: function cart(product) {
      var _this6 = this;

      if (product.quantity > product.units) {
        this.$store.state.message = "Only available " + product.units + " units";
        this.swall();
      } else {
        this.$store.dispatch('addCart', product).then(function (response) {
          _this6.swall();

          setTimeout(function () {
            _this6.check(_this6.product);
          }, 1200);
        });
      }
    },
    swall: function swall() {
      var _this7 = this;

      setTimeout(function () {
        sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
          position: 'top-end',
          icon: 'warning',
          title: _this7.callMessage,
          showConfirmButton: false,
          timer: 1000
        });
      }, 1100);
    },
    AdvanceSearch: function AdvanceSearch() {
      var _this8 = this;

      var min_price = this.min_price;
      var max_price = this.max_price;
      var size = this.size;
      var brend = this.brend[0];
      var category = this.kategorija;
      var color = this.boja;
      var uri = '/api/filter';
      axios.post(uri, {
        brend: brend,
        category: category,
        min_price: min_price,
        max_price: max_price,
        size: size,
        color: color
      }, {
        headers: {
          'Access-Control-Allow-Origin': '*'
        }
      }).then(function (response) {
        _this8.advancedfilter = true;
        var products = response.data.product;

        _this8.$store.dispatch('filter', products);
      });
    }
  })
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/SingleProduct.vue?vue&type=style&index=0&id=350e3386&scoped=true&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/SingleProduct.vue?vue&type=style&index=0&id=350e3386&scoped=true&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.small-text[data-v-350e3386] {\n\tfont-size: 18px;\n}\n.title[data-v-350e3386] {\n\tfont-size: 36px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/SingleProduct.vue?vue&type=style&index=0&id=350e3386&scoped=true&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/SingleProduct.vue?vue&type=style&index=0&id=350e3386&scoped=true&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./SingleProduct.vue?vue&type=style&index=0&id=350e3386&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/SingleProduct.vue?vue&type=style&index=0&id=350e3386&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/SingleProduct.vue?vue&type=template&id=350e3386&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/SingleProduct.vue?vue&type=template&id=350e3386&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container", attrs: { id: "rout" } }, [
    _c("section", { staticClass: "product-shop spad page-details" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-lg-3" }),
          _vm._v(" "),
          _c("div", { staticClass: "col-lg-9" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-lg-6" }, [
                _c("div", { staticClass: "product-pic-zoom" }, [
                  _c("img", {
                    staticClass: "product-big-img",
                    attrs: { src: _vm.image_src + _vm.product.image, alt: "" }
                  }),
                  _vm._v(" "),
                  _c("br"),
                  _c("br"),
                  _vm._v(" "),
                  _c("div", {
                    domProps: { innerHTML: _vm._s(_vm.product.description) }
                  })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-6" }, [
                _c("div", { staticClass: "product-details" }, [
                  _c(
                    "div",
                    { staticClass: "pd-title" },
                    [
                      _vm._l(_vm.allCategories, function(category, index) {
                        return _c("div", { on: { key: index } }, [
                          category.id == _vm.product.category_id
                            ? _c("span", [_vm._v(_vm._s(category.kategorija))])
                            : _vm._e()
                        ])
                      }),
                      _vm._v(" "),
                      _c("h3", [_vm._v(_vm._s(_vm.product.name))]),
                      _vm._v(" "),
                      _vm._m(0)
                    ],
                    2
                  ),
                  _vm._v(" "),
                  _vm._m(1),
                  _vm._v(" "),
                  _c("div", { staticClass: "pd-desc" }, [
                    _c("p"),
                    _vm._v(" "),
                    _c("h4", [_vm._v("$ " + _vm._s(_vm.product.price))])
                  ]),
                  _vm._v(" "),
                  _c("div", [
                    _c("h4", [_vm._v("Stock")]),
                    _vm._v(" "),
                    _c("h6", [
                      _vm._v(_vm._s(_vm.product.units) + " units available")
                    ]),
                    _vm._v(" "),
                    _c("br")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "quantity" }, [
                    _c("div", { staticClass: "pro-qty" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.product.quantity,
                            expression: "product.quantity"
                          }
                        ],
                        attrs: { type: "text" },
                        domProps: { value: _vm.product.quantity },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.product,
                              "quantity",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    !_vm.alreadyInCart
                      ? _c(
                          "button",
                          {
                            staticClass: "primary-btn pd-cart",
                            on: {
                              click: function($event) {
                                $event.preventDefault()
                                return _vm.cart(_vm.product)
                              }
                            }
                          },
                          [_vm._v("Add To Cart")]
                        )
                      : _c(
                          "button",
                          {
                            staticClass: "primary-btn pd-cart",
                            on: {
                              click: function($event) {
                                $event.preventDefault()
                                return _vm.quantity(_vm.product)
                              }
                            }
                          },
                          [_vm._v("Update Cart")]
                        )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "float-right" }, [
                    _vm.alreadyInCart
                      ? _c(
                          "button",
                          {
                            staticClass: "site-btn place-btn",
                            attrs: { type: "button" },
                            on: {
                              click: function($event) {
                                $event.preventDefault()
                                return _vm.remove(_vm.product)
                              }
                            }
                          },
                          [_vm._v("Remove from Cart")]
                        )
                      : _vm._e()
                  ]),
                  _vm._v(" "),
                  _c("ul", { staticClass: "pd-tags" }, [
                    _c(
                      "li",
                      [
                        _c("span", [_vm._v("CATEGORY")]),
                        _vm._v(": "),
                        _vm._l(_vm.allCategories, function(cat, index) {
                          return _c("div", { on: { key: index } }, [
                            cat.id == _vm.product.category_id
                              ? _c("p", [_vm._v(_vm._s(cat.kategorija))])
                              : _vm._e()
                          ])
                        })
                      ],
                      2
                    ),
                    _vm._v(" "),
                    _c(
                      "li",
                      [
                        _c("span", [_vm._v("TAGS")]),
                        _vm._v(": "),
                        _vm._l(_vm.allTags, function(tag, index) {
                          return _c("div", { on: { key: index } }, [
                            tag.id == _vm.product.tag_id
                              ? _c("p", [_vm._v(_vm._s(tag.tag))])
                              : _vm._e()
                          ])
                        })
                      ],
                      2
                    )
                  ]),
                  _vm._v(" "),
                  _vm._m(2)
                ])
              ])
            ]),
            _vm._v(" "),
            _vm._m(3)
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "related-products spad" }, [
      _c("div", { staticClass: "container" }, [
        _vm._m(4),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "row" },
          _vm._l(_vm.computedObj, function(product) {
            return _c("div", { staticClass: "col-lg-3 col-sm-6" }, [
              _c("div", { staticClass: "product-item" }, [
                _c(
                  "div",
                  { staticClass: "pi-pic" },
                  [
                    _c("img", {
                      attrs: { src: _vm.image_src + product.image, alt: "" }
                    }),
                    _vm._v(" "),
                    _vm._l(_vm.allTags, function(tag, index) {
                      return _c(
                        "div",
                        { staticClass: "sale pp-sale", on: { key: index } },
                        [
                          tag.id == product.tag_id
                            ? _c("span", [_vm._v(_vm._s(tag.tag))])
                            : _vm._e()
                        ]
                      )
                    }),
                    _vm._v(" "),
                    _c("div", { staticClass: "icon" }, [
                      _c("i", {
                        staticClass: "fa fa-heart-o",
                        on: {
                          click: function($event) {
                            return _vm.like(product)
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("ul", [
                      _c("li", { staticClass: "w-icon active" }, [
                        _c("a", [
                          _c("i", {
                            staticClass: "fa fa-shopping-bag",
                            on: {
                              click: function($event) {
                                $event.preventDefault()
                                return _vm.cart(product)
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("li", { staticClass: "quick-view" }, [
                        _c(
                          "a",
                          {
                            attrs: {
                              href: _vm.$router.resolve({
                                name: "single-products",
                                params: { id: product.id }
                              }).href,
                              v: ""
                            }
                          },
                          [_vm._v("+ Quick View")]
                        )
                      ]),
                      _vm._v(" "),
                      _vm._m(5, true)
                    ])
                  ],
                  2
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "pi-text" },
                  [
                    _vm._l(_vm.allCategories, function(category, index) {
                      return _c(
                        "div",
                        { staticClass: "catagory-name", on: { key: index } },
                        [
                          category.id == product.category_id
                            ? _c("span", [_vm._v(_vm._s(category.kategorija))])
                            : _vm._e()
                        ]
                      )
                    }),
                    _vm._v(" "),
                    _c("h4", [_c("span", [_vm._v(_vm._s(product.name))])]),
                    _vm._v(" "),
                    _c("div", { staticClass: "product-price" }, [
                      _vm._v(
                        "\n\t\t\t\t\t\t\t\t\t  " +
                          _vm._s(product.price) +
                          "\n\t\t\t\t\t\t\t\t "
                      )
                    ])
                  ],
                  2
                )
              ])
            ])
          }),
          0
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { staticClass: "heart-icon", attrs: { href: "#" } }, [
      _c("i", { staticClass: "icon_heart_alt" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "pd-rating" }, [
      _c("i", { staticClass: "fa fa-star" }),
      _vm._v(" "),
      _c("i", { staticClass: "fa fa-star" }),
      _vm._v(" "),
      _c("i", { staticClass: "fa fa-star" }),
      _vm._v(" "),
      _c("i", { staticClass: "fa fa-star" }),
      _vm._v(" "),
      _c("i", { staticClass: "fa fa-star-o" }),
      _vm._v(" "),
      _c("span", [_vm._v("(5)")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "pd-share" }, [
      _c("div", { staticClass: "p-code" }, [_vm._v("Sku : 00012")]),
      _vm._v(" "),
      _c("div", { staticClass: "pd-social" }, [
        _c("a", { attrs: { href: "#" } }, [
          _c("i", { staticClass: "ti-facebook" })
        ]),
        _vm._v(" "),
        _c("a", { attrs: { href: "#" } }, [
          _c("i", { staticClass: "ti-twitter-alt" })
        ]),
        _vm._v(" "),
        _c("a", { attrs: { href: "#" } }, [
          _c("i", { staticClass: "ti-linkedin" })
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "product-tab" }, [
      _c("div", { staticClass: "tab-item" }, [
        _c("ul", { staticClass: "nav", attrs: { role: "tablist" } }, [
          _c("li", [
            _c(
              "a",
              {
                staticClass: "active",
                attrs: { "data-toggle": "tab", href: "#tab-1", role: "tab" }
              },
              [_vm._v("DESCRIPTION")]
            )
          ]),
          _vm._v(" "),
          _c("li", [
            _c(
              "a",
              { attrs: { "data-toggle": "tab", href: "#tab-2", role: "tab" } },
              [_vm._v("SPECIFICATIONS")]
            )
          ]),
          _vm._v(" "),
          _c("li", [
            _c(
              "a",
              { attrs: { "data-toggle": "tab", href: "#tab-3", role: "tab" } },
              [_vm._v("Customer Reviews (02)")]
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-lg-7" }, [
          _c("h5", [_vm._v("Introduction")]),
          _vm._v(" "),
          _c("p", [
            _vm._v(
              "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do\n                                              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim\n                                               ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut\n                                               aliquip ex ea commodo consequat. Duis aute irure dolor in "
            )
          ]),
          _vm._v(" "),
          _c("h5", [_vm._v("Features")]),
          _vm._v(" "),
          _c("p", [
            _vm._v(
              "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do\n                                                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim\n                                                    ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut\n                                                    aliquip ex ea commodo consequat. Duis aute irure dolor in "
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-lg-5" })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-lg-12" }, [
        _c("div", { staticClass: "section-title" }, [
          _c("h2", [_vm._v("Recent Products")])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "w-icon" }, [
      _c("a", { attrs: { href: "" } }, [
        _c("i", { staticClass: "fa fa-random" })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/front/SingleProduct.vue":
/*!*********************************************************!*\
  !*** ./resources/js/components/front/SingleProduct.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SingleProduct_vue_vue_type_template_id_350e3386_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SingleProduct.vue?vue&type=template&id=350e3386&scoped=true& */ "./resources/js/components/front/SingleProduct.vue?vue&type=template&id=350e3386&scoped=true&");
/* harmony import */ var _SingleProduct_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SingleProduct.vue?vue&type=script&lang=js& */ "./resources/js/components/front/SingleProduct.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _SingleProduct_vue_vue_type_style_index_0_id_350e3386_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./SingleProduct.vue?vue&type=style&index=0&id=350e3386&scoped=true&lang=css& */ "./resources/js/components/front/SingleProduct.vue?vue&type=style&index=0&id=350e3386&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _SingleProduct_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SingleProduct_vue_vue_type_template_id_350e3386_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SingleProduct_vue_vue_type_template_id_350e3386_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "350e3386",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/front/SingleProduct.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/front/SingleProduct.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/front/SingleProduct.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SingleProduct_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./SingleProduct.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/SingleProduct.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SingleProduct_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/front/SingleProduct.vue?vue&type=style&index=0&id=350e3386&scoped=true&lang=css&":
/*!******************************************************************************************************************!*\
  !*** ./resources/js/components/front/SingleProduct.vue?vue&type=style&index=0&id=350e3386&scoped=true&lang=css& ***!
  \******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SingleProduct_vue_vue_type_style_index_0_id_350e3386_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./SingleProduct.vue?vue&type=style&index=0&id=350e3386&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/SingleProduct.vue?vue&type=style&index=0&id=350e3386&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SingleProduct_vue_vue_type_style_index_0_id_350e3386_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SingleProduct_vue_vue_type_style_index_0_id_350e3386_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SingleProduct_vue_vue_type_style_index_0_id_350e3386_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SingleProduct_vue_vue_type_style_index_0_id_350e3386_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SingleProduct_vue_vue_type_style_index_0_id_350e3386_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/front/SingleProduct.vue?vue&type=template&id=350e3386&scoped=true&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/components/front/SingleProduct.vue?vue&type=template&id=350e3386&scoped=true& ***!
  \****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SingleProduct_vue_vue_type_template_id_350e3386_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./SingleProduct.vue?vue&type=template&id=350e3386&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/SingleProduct.vue?vue&type=template&id=350e3386&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SingleProduct_vue_vue_type_template_id_350e3386_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SingleProduct_vue_vue_type_template_id_350e3386_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);