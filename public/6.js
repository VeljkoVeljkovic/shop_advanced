(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[6],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/check-out.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/check-out.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.js */ "./node_modules/sweetalert2/dist/sweetalert2.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2/src/sweetalert2.scss */ "./node_modules/sweetalert2/src/sweetalert2.scss");
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_2__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      coupon: '',
      regy: false,
      image_src: '/uploads/products/',
      firstname: "",
      lastname: "",
      companyname: "",
      country: "",
      streetaddress: "",
      postcode: "",
      town: "",
      email: "",
      phone: "",
      password: "",
      password_confirmation: "",
      product: [],
      error: ""
    };
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])(['allCart', 'allCoupons', 'allTags', 'userdata', 'callMessage']), {
    total: function total() {
      return this.$store.getters.cardTotal;
    }
  }),
  methods: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])(["cart", "deleteCart"]), {
    User: function User(user) {
      this.$store.dispatch('User', user);
    },
    swall: function swall() {
      var _this = this;

      setTimeout(function () {
        sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
          position: 'top-end',
          icon: 'warning',
          title: _this.callMessage,
          showConfirmButton: false,
          timer: 1000
        });
      }, 1100);
    },
    couponcheck: function couponcheck() {
      var _this2 = this;

      if (!this.userdata.firstname) {
        this.$store.state.message = "You are not logged in";
        this.swall();
      } else {
        var uri = '/api/check';
        var id = this.userdata.id;
        var coupon = this.allCoupons.coupongenerator;
        axios.post(uri, {
          id: id,
          coupon: coupon
        }, {
          headers: {
            'Access-Control-Allow-Origin': '*'
          }
        }).then(function (response) {
          if (response.data.coupon != null) {
            _this2.$store.dispatch('couponData', response.data.coupon);

            _this2.$store.dispatch('discountCart', response.data.coupon);

            _this2.$store.state.message = "The coupon is valid";

            _this2.swall();
          } else {
            _this2.$store.state.message = "Make sure you enter the correct coupon code";

            _this2.swall();
          }
        });
      }
    },
    reg: function reg() {
      this.regy = true;
    },
    cancelreg: function cancelreg() {
      this.regy = false;
    },
    register: function register() {
      if (localStorage.getItem('shop.mdi.in.rs.jwt') == null) {
        this.$emit('loggedIn');
        this.$router.push({
          name: 'register',
          params: {
            nextUrl: this.$route.fullPath
          }
        });
      }
    },
    addUser: function addUser(user) {
      this.$store.dispatch('addUser', user);
    },
    handleSubmit: function handleSubmit() {
      var _this3 = this;

      this.$validator.validateAll().then(function (result) {
        if (!result) {
          _this3.error = "Proverite da li ste uneli validne podatke!";
          return;
        }

        var id = _this3.userdata.id;
        var firstname = _this3.userdata.firstname;
        var lastname = _this3.userdata.lastname;
        var companyname = _this3.userdata.companyname;
        var country = _this3.userdata.country;
        var streetaddress = _this3.userdata.streetaddress;
        var postcode = _this3.userdata.postcode;
        var town = _this3.userdata.town;
        var email = _this3.userdata.email;
        var phone = _this3.userdata.phone;

        if (_this3.password) {
          var _password = _this3.password;
        } else {
          var _password2 = 0;
        }

        var product = _this3.allCart;
        var coupon = _this3.allCoupons.coupons_id;
        axios.post('api/order', {
          id: id,
          firstname: firstname,
          lastname: lastname,
          companyname: companyname,
          country: country,
          streetaddress: streetaddress,
          postcode: postcode,
          town: town,
          email: email,
          phone: phone,
          product: product,
          password: password,
          coupon: coupon
        }, {
          headers: {
            'Access-Control-Allow-Origin': '*'
          }
        }).then(function (response) {
          // let user = response.data.user
          // this.User(user)
          _this3.deleteCart();

          _this3.$router.push('/confirmation');
        });
      });
    }
  })
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/check-out.vue?vue&type=style&index=0&id=fc8178b0&scoped=true&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/check-out.vue?vue&type=style&index=0&id=fc8178b0&scoped=true&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.proba[data-v-fc8178b0] {\n     color: #E7AB3C;\n\t text-decoration: line-through;\n\t -webkit-text-decoration-color: black;\n\t         text-decoration-color: black;\n\t -webkit-text-decoration-style: double;\n\t         text-decoration-style: double;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/check-out.vue?vue&type=style&index=0&id=fc8178b0&scoped=true&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/check-out.vue?vue&type=style&index=0&id=fc8178b0&scoped=true&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./check-out.vue?vue&type=style&index=0&id=fc8178b0&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/check-out.vue?vue&type=style&index=0&id=fc8178b0&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/check-out.vue?vue&type=template&id=fc8178b0&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/check-out.vue?vue&type=template&id=fc8178b0&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { attrs: { id: "rout" } }, [
    _c("section", { staticClass: "checkout-section spad" }, [
      _c("div", { staticClass: "container" }, [
        _c(
          "form",
          {
            staticClass: "checkout-form",
            on: {
              submit: function($event) {
                $event.preventDefault()
                return _vm.handleSubmit($event)
              }
            }
          },
          [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-lg-6" }, [
                _c(
                  "div",
                  { staticClass: "checkout-content" },
                  [
                    _c(
                      "router-link",
                      {
                        staticClass: "content-btn",
                        attrs: { to: { name: "login" } }
                      },
                      [_vm._v("Click Here To Login")]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c("h4", [_vm._v("Biiling Details")]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-lg-6" }, [
                    _vm._m(0),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.userdata.firstname,
                          expression: "userdata.firstname"
                        },
                        {
                          name: "validate",
                          rawName: "v-validate",
                          value: "required",
                          expression: "'required'"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { name: "firstname", type: "text" },
                      domProps: { value: _vm.userdata.firstname },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.userdata,
                            "firstname",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.errors.has("firstname"),
                            expression: "errors.has('firstname')"
                          }
                        ],
                        staticClass: "help-block alert alert-danger"
                      },
                      [
                        _vm._v(
                          "\n\t\t\t\t\t\t\t\t\t " +
                            _vm._s(_vm.errors.first("firstname")) +
                            "\n\t\t\t\t\t\t\t\t\t"
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-lg-6" }, [
                    _vm._m(1),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.userdata.lastname,
                          expression: "userdata.lastname"
                        },
                        {
                          name: "validate",
                          rawName: "v-validate",
                          value: "required",
                          expression: "'required'"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { name: "lastname", type: "text" },
                      domProps: { value: _vm.userdata.lastname },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.userdata,
                            "lastname",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.errors.has("lastname"),
                            expression: "errors.has('lastname')"
                          }
                        ],
                        staticClass: "help-block alert alert-danger"
                      },
                      [
                        _vm._v(
                          "\n\t\t\t\t\t\t\t\t\t " +
                            _vm._s(_vm.errors.first("lastname")) +
                            "\n\t\t\t\t\t\t\t\t\t"
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-lg-12" }, [
                    _c(
                      "label",
                      {
                        staticClass: "col-form-label text-md-right",
                        attrs: { for: "companyname" }
                      },
                      [_vm._v("Company Name")]
                    ),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.userdata.companyname,
                          expression: "userdata.companyname"
                        },
                        {
                          name: "validate",
                          rawName: "v-validate",
                          value: "required",
                          expression: "'required'"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { name: "companyname", type: "text" },
                      domProps: { value: _vm.userdata.companyname },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.userdata,
                            "companyname",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.errors.has("companyname"),
                            expression: "errors.has('companyname')"
                          }
                        ],
                        staticClass: "help-block alert alert-danger"
                      },
                      [
                        _vm._v(
                          "\n\t\t\t\t\t\t\t\t\t " +
                            _vm._s(_vm.errors.first("companyname")) +
                            "\n\t\t\t\t\t\t\t\t\t"
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-lg-12" }, [
                    _vm._m(2),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.userdata.country,
                          expression: "userdata.country"
                        },
                        {
                          name: "validate",
                          rawName: "v-validate",
                          value: "required",
                          expression: "'required'"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { name: "country", type: "text" },
                      domProps: { value: _vm.userdata.country },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.userdata, "country", $event.target.value)
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.errors.has("country"),
                            expression: "errors.has('country')"
                          }
                        ],
                        staticClass: "help-block alert alert-danger"
                      },
                      [
                        _vm._v(
                          "\n\t\t\t\t\t\t\t\t\t " +
                            _vm._s(_vm.errors.first("country")) +
                            "\n\t\t\t\t\t\t\t\t\t"
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-lg-12" }, [
                    _vm._m(3),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.userdata.streetaddress,
                          expression: "userdata.streetaddress"
                        },
                        {
                          name: "validate",
                          rawName: "v-validate",
                          value: "required",
                          expression: "'required'"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { name: "streetaddress", type: "text" },
                      domProps: { value: _vm.userdata.streetaddress },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.userdata,
                            "streetaddress",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.errors.has("streetaddress"),
                            expression: "errors.has('streetaddress')"
                          }
                        ],
                        staticClass: "help-block alert alert-danger"
                      },
                      [
                        _vm._v(
                          "\n\t\t\t\t\t\t\t\t\t " +
                            _vm._s(_vm.errors.first("streetaddress")) +
                            "\n\t\t\t\t\t\t\t\t\t"
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-lg-6" }, [
                    _vm._m(4),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.userdata.postcode,
                          expression: "userdata.postcode"
                        },
                        {
                          name: "validate",
                          rawName: "v-validate",
                          value: "required",
                          expression: "'required'"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { name: "postcode", type: "text", id: "zip" },
                      domProps: { value: _vm.userdata.postcode },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.userdata,
                            "postcode",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.errors.has("postcode"),
                            expression: "errors.has('postcode')"
                          }
                        ],
                        staticClass: "help-block alert alert-danger"
                      },
                      [
                        _vm._v(
                          "\n\t\t\t\t\t\t\t\t\t " +
                            _vm._s(_vm.errors.first("postcode")) +
                            "\n\t\t\t\t\t\t\t\t\t"
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-lg-6" }, [
                    _vm._m(5),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.userdata.town,
                          expression: "userdata.town"
                        },
                        {
                          name: "validate",
                          rawName: "v-validate",
                          value: "required",
                          expression: "'required'"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { name: "town", type: "text", id: "town" },
                      domProps: { value: _vm.userdata.town },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.userdata, "town", $event.target.value)
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.errors.has("town"),
                            expression: "errors.has('town')"
                          }
                        ],
                        staticClass: "help-block alert alert-danger"
                      },
                      [
                        _vm._v(
                          "\n\t\t\t\t\t\t\t\t\t " +
                            _vm._s(_vm.errors.first("town")) +
                            "\n\t\t\t\t\t\t\t\t\t"
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-lg-6" }, [
                    _vm._m(6),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.userdata.email,
                          expression: "userdata.email"
                        },
                        {
                          name: "validate",
                          rawName: "v-validate",
                          value: "required",
                          expression: "'required'"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { name: "email", type: "text", id: "email" },
                      domProps: { value: _vm.userdata.email },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.userdata, "email", $event.target.value)
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.errors.has("email"),
                            expression: "errors.has('email')"
                          }
                        ],
                        staticClass: "help-block alert alert-danger"
                      },
                      [
                        _vm._v(
                          "\n\t\t\t\t\t\t\t\t\t " +
                            _vm._s(_vm.errors.first("email")) +
                            "\n\t\t\t\t\t\t\t\t\t"
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-lg-6" }, [
                    _vm._m(7),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.userdata.phone,
                          expression: "userdata.phone"
                        },
                        {
                          name: "validate",
                          rawName: "v-validate",
                          value: "required",
                          expression: "'required'"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { name: "phone", type: "text", id: "phone" },
                      domProps: { value: _vm.userdata.phone },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.userdata, "phone", $event.target.value)
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.errors.has("phone"),
                            expression: "errors.has('phone')"
                          }
                        ],
                        staticClass: "help-block alert alert-danger"
                      },
                      [
                        _vm._v(
                          "\n\t\t\t\t\t\t\t\t\t " +
                            _vm._s(_vm.errors.first("phone")) +
                            "\n                                "
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  !_vm.regy && _vm.userdata.length == 0
                    ? _c("div", { staticClass: "col-lg-12" }, [
                        _c("div", { staticClass: "create-item" }, [
                          _c("label", { attrs: { for: "acc-create" } }, [
                            _vm._v(
                              "\n                                        Create an account?\n                                        "
                            ),
                            _c("input", {
                              attrs: { type: "checkbox", id: "acc-create" },
                              on: { click: _vm.reg }
                            }),
                            _vm._v(" "),
                            _c("span", { staticClass: "checkmark" })
                          ])
                        ])
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.regy
                    ? _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "col-lg-6" }, [
                          _vm._m(8),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model.trim",
                                value: _vm.password,
                                expression: "password",
                                modifiers: { trim: true }
                              },
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "min:6",
                                expression: "'min:6'"
                              }
                            ],
                            ref: "password",
                            staticClass: "form-control",
                            attrs: { name: "password", type: "password" },
                            domProps: { value: _vm.password },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.password = $event.target.value.trim()
                              },
                              blur: function($event) {
                                return _vm.$forceUpdate()
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.errors.has("password"),
                                  expression: "errors.has('password')"
                                }
                              ],
                              staticClass: "help-block alert alert-danger"
                            },
                            [
                              _vm._v(
                                "\n\t\t\t\t\t\t\t\t\t " +
                                  _vm._s(_vm.errors.first("password")) +
                                  "\n\t\t\t\t\t\t\t\t\t"
                              )
                            ]
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-lg-6" }, [
                          _vm._m(9),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model.trim",
                                value: _vm.password_confirmation,
                                expression: "password_confirmation",
                                modifiers: { trim: true }
                              },
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "min:6|confirmed:password",
                                expression: "'min:6|confirmed:password'"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              name: "password_confirmation",
                              "data-vv-as": "password",
                              type: "password"
                            },
                            domProps: { value: _vm.password_confirmation },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.password_confirmation = $event.target.value.trim()
                              },
                              blur: function($event) {
                                return _vm.$forceUpdate()
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.errors.has(
                                    "password_confirmation"
                                  ),
                                  expression:
                                    "errors.has('password_confirmation')"
                                }
                              ],
                              staticClass: "help-block alert alert-danger"
                            },
                            [
                              _vm._v(
                                "\n\t\t\t\t\t\t\t\t\t " +
                                  _vm._s(
                                    _vm.errors.first("password_confirmation")
                                  ) +
                                  "\n\t\t\t\t\t\t\t\t\t"
                              )
                            ]
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            staticClass: "site-btn place-btn",
                            attrs: { type: "submit" },
                            on: { click: _vm.cancelreg }
                          },
                          [_vm._v("Cancel registration")]
                        )
                      ])
                    : _vm._e()
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-lg-6" }, [
                _c(
                  "form",
                  {
                    on: {
                      submit: function($event) {
                        $event.preventDefault()
                        return _vm.couponcheck($event)
                      }
                    }
                  },
                  [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model.trim",
                          value: _vm.allCoupons.coupongenerator,
                          expression: "allCoupons.coupongenerator",
                          modifiers: { trim: true }
                        },
                        {
                          name: "validate",
                          rawName: "v-validate",
                          value: "min:6",
                          expression: "'min:6'"
                        }
                      ],
                      attrs: {
                        type: "text",
                        placeholder: "Enter Your Coupon Code"
                      },
                      domProps: { value: _vm.allCoupons.coupongenerator },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.allCoupons,
                            "coupongenerator",
                            $event.target.value.trim()
                          )
                        },
                        blur: function($event) {
                          return _vm.$forceUpdate()
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "site-btn place-btn",
                        attrs: { type: "submit" }
                      },
                      [_vm._v("Place Coupon")]
                    )
                  ]
                ),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("div", { staticClass: "place-order" }, [
                  _c("h4", [_vm._v("Your Order")]),
                  _vm._v(" "),
                  _c("div", { staticClass: "order-total" }, [
                    _c(
                      "ul",
                      { staticClass: "order-table" },
                      [
                        _vm._m(10),
                        _vm._v(" "),
                        _vm._l(_vm.allCart, function(product, index) {
                          return _c(
                            "li",
                            {
                              staticClass: "fw-normal",
                              on: { key: index },
                              model: {
                                value: _vm.userdata.product,
                                callback: function($$v) {
                                  _vm.$set(_vm.userdata, "product", $$v)
                                },
                                expression: "userdata.product"
                              }
                            },
                            [
                              _vm._v(
                                "\n\t\t\t\t\t\t\t\t\t\t " +
                                  _vm._s(product.name) +
                                  " x " +
                                  _vm._s(product.quantity) +
                                  " "
                              ),
                              _c(
                                "span",
                                { class: _vm.allCoupons != 0 ? "proba" : "" },
                                [
                                  _vm._v(
                                    " $" +
                                      _vm._s(product.price * product.quantity)
                                  )
                                ]
                              ),
                              _vm._v("\n\t\t\t\t\t\t\t\t\t\t   "),
                              _vm.allCoupons != 0
                                ? _c("span", [
                                    _vm._v(
                                      "\n\t\t\t\t\t\t\t\t\t\t      $" +
                                        _vm._s(
                                          product.discount_price *
                                            product.quantity
                                        ) +
                                        "\n\t\t\t\t\t\t\t\t\t\t "
                                    )
                                  ])
                                : _vm._e()
                            ]
                          )
                        }),
                        _vm._v(" "),
                        _c("li", { staticClass: "fw-normal" }, [
                          _vm._v("Subtotal "),
                          _c("span", [_vm._v("$" + _vm._s(_vm.total))])
                        ]),
                        _vm._v(" "),
                        _c("li", { staticClass: "total-price" }, [
                          _vm._v("Total "),
                          _c("span", [_vm._v("$" + _vm._s(_vm.total))])
                        ])
                      ],
                      2
                    ),
                    _vm._v(" "),
                    _vm._m(11)
                  ])
                ])
              ])
            ])
          ]
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "label",
      {
        staticClass: "col-form-label text-md-right",
        attrs: { for: "firstname" }
      },
      [_vm._v("First Name"), _c("span", [_vm._v("*")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "label",
      {
        staticClass: "col-form-label text-md-right",
        attrs: { for: "lastname" }
      },
      [_vm._v("Last Name"), _c("span", [_vm._v("*")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "label",
      {
        staticClass: "col-form-label text-md-right",
        attrs: { for: "country" }
      },
      [_vm._v("Country"), _c("span", [_vm._v("*")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "label",
      {
        staticClass: "col-form-label text-md-right",
        attrs: { for: "streetaddress" }
      },
      [_vm._v("Street Address"), _c("span", [_vm._v("*")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "label",
      {
        staticClass: "col-form-label text-md-right",
        attrs: { for: "postcode" }
      },
      [_vm._v("Postcode / ZIP (optional)"), _c("span", [_vm._v("*")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "label",
      { staticClass: "col-form-label text-md-right", attrs: { for: "town" } },
      [_vm._v("Town / City"), _c("span", [_vm._v("*")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "label",
      { staticClass: "col-form-label text-md-right", attrs: { for: "email" } },
      [_vm._v("Email Address"), _c("span", [_vm._v("*")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "label",
      { staticClass: "col-form-label text-md-right", attrs: { for: "phone" } },
      [_vm._v("Phone"), _c("span", [_vm._v("*")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "label",
      {
        staticClass: "col-form-label text-md-right",
        attrs: { for: "password" }
      },
      [_vm._v("Password"), _c("span", [_vm._v("*")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "label",
      {
        staticClass: "col-form-label text-md-right",
        attrs: { for: "password-confirm" }
      },
      [_vm._v("Confirm Password"), _c("span", [_vm._v("*")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", [_vm._v("Product "), _c("span", [_vm._v("Total")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "order-btn" }, [
      _c(
        "button",
        { staticClass: "site-btn place-btn", attrs: { type: "submit" } },
        [_vm._v("Place Order")]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/front/check-out.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/front/check-out.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _check_out_vue_vue_type_template_id_fc8178b0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./check-out.vue?vue&type=template&id=fc8178b0&scoped=true& */ "./resources/js/components/front/check-out.vue?vue&type=template&id=fc8178b0&scoped=true&");
/* harmony import */ var _check_out_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./check-out.vue?vue&type=script&lang=js& */ "./resources/js/components/front/check-out.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _check_out_vue_vue_type_style_index_0_id_fc8178b0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./check-out.vue?vue&type=style&index=0&id=fc8178b0&scoped=true&lang=css& */ "./resources/js/components/front/check-out.vue?vue&type=style&index=0&id=fc8178b0&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _check_out_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _check_out_vue_vue_type_template_id_fc8178b0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _check_out_vue_vue_type_template_id_fc8178b0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "fc8178b0",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/front/check-out.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/front/check-out.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/front/check-out.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_check_out_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./check-out.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/check-out.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_check_out_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/front/check-out.vue?vue&type=style&index=0&id=fc8178b0&scoped=true&lang=css&":
/*!**************************************************************************************************************!*\
  !*** ./resources/js/components/front/check-out.vue?vue&type=style&index=0&id=fc8178b0&scoped=true&lang=css& ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_check_out_vue_vue_type_style_index_0_id_fc8178b0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./check-out.vue?vue&type=style&index=0&id=fc8178b0&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/check-out.vue?vue&type=style&index=0&id=fc8178b0&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_check_out_vue_vue_type_style_index_0_id_fc8178b0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_check_out_vue_vue_type_style_index_0_id_fc8178b0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_check_out_vue_vue_type_style_index_0_id_fc8178b0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_check_out_vue_vue_type_style_index_0_id_fc8178b0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_check_out_vue_vue_type_style_index_0_id_fc8178b0_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/front/check-out.vue?vue&type=template&id=fc8178b0&scoped=true&":
/*!************************************************************************************************!*\
  !*** ./resources/js/components/front/check-out.vue?vue&type=template&id=fc8178b0&scoped=true& ***!
  \************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_check_out_vue_vue_type_template_id_fc8178b0_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./check-out.vue?vue&type=template&id=fc8178b0&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/check-out.vue?vue&type=template&id=fc8178b0&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_check_out_vue_vue_type_template_id_fc8178b0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_check_out_vue_vue_type_template_id_fc8178b0_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);