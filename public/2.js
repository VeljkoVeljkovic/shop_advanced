(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Admin.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Admin.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _admin_Products_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./admin/Products.vue */ "./resources/js/components/admin/Products.vue");
/* harmony import */ var _admin_Main_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin/Main.vue */ "./resources/js/components/admin/Main.vue");
/* harmony import */ var _admin_Users__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./admin/Users */ "./resources/js/components/admin/Users.vue");
/* harmony import */ var _admin_Category_vue__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./admin/Category.vue */ "./resources/js/components/admin/Category.vue");
/* harmony import */ var _admin_Brand_vue__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./admin/Brand.vue */ "./resources/js/components/admin/Brand.vue");
/* harmony import */ var _admin_Tag_vue__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./admin/Tag.vue */ "./resources/js/components/admin/Tag.vue");
/* harmony import */ var _admin_Orders_vue__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./admin/Orders.vue */ "./resources/js/components/admin/Orders.vue");
/* harmony import */ var _admin_Coupons_vue__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./admin/Coupons.vue */ "./resources/js/components/admin/Coupons.vue");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//









/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      show: false,
      name: null,
      user_type: 0,
      user: null,
      activeComponent: null,
      isLoggedIn: localStorage.getItem('shop.mdi.in.rs.jwt') != null
    };
  },
  components: {
    Main: _admin_Main_vue__WEBPACK_IMPORTED_MODULE_2__["default"],
    Users: _admin_Users__WEBPACK_IMPORTED_MODULE_3__["default"],
    Category: _admin_Category_vue__WEBPACK_IMPORTED_MODULE_4__["default"],
    Product: _admin_Products_vue__WEBPACK_IMPORTED_MODULE_1__["default"],
    Brand: _admin_Brand_vue__WEBPACK_IMPORTED_MODULE_5__["default"],
    Orders: _admin_Orders_vue__WEBPACK_IMPORTED_MODULE_7__["default"],
    Tag: _admin_Tag_vue__WEBPACK_IMPORTED_MODULE_6__["default"],
    Coupons: _admin_Coupons_vue__WEBPACK_IMPORTED_MODULE_8__["default"]
  },
  beforeMount: function beforeMount() {
    this.setComponent(this.$route.params.page);
    this.user = JSON.parse(localStorage.getItem('shop.mdi.in.rs.  d fuser'));
    axios.defaults.headers.common['Content-Type'] = 'application/json';
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('shop.mdi.in.rs.jwt');

    if (this.isLoggedIn) {
      var user = JSON.parse(localStorage.getItem('shop.mdi.in.rs.user'));
      this.name = user.name;
      this.user_type = user.is_admin;
    }
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])(['userdata'])),
  methods: {
    setComponent: function setComponent(value) {
      switch (value) {
        case "users":
          this.activeComponent = _admin_Users__WEBPACK_IMPORTED_MODULE_3__["default"];
          this.$router.push({
            name: 'admin-pages',
            params: {
              page: 'users'
            }
          });
          break;

        case "brand":
          this.activeComponent = _admin_Brand_vue__WEBPACK_IMPORTED_MODULE_5__["default"];
          this.$router.push({
            name: 'admin-pages',
            params: {
              page: 'brand'
            }
          });
          break;

        case "tag":
          this.activeComponent = _admin_Tag_vue__WEBPACK_IMPORTED_MODULE_6__["default"];
          this.$router.push({
            name: 'admin-pages',
            params: {
              page: 'tag'
            }
          });
          break;

        case "orders":
          this.activeComponent = _admin_Orders_vue__WEBPACK_IMPORTED_MODULE_7__["default"];
          this.$router.push({
            name: 'admin-pages',
            params: {
              page: 'orders'
            }
          });
          break;

        case "products":
          this.activeComponent = _admin_Products_vue__WEBPACK_IMPORTED_MODULE_1__["default"];
          this.$router.push({
            name: 'admin-pages',
            params: {
              page: 'products'
            }
          });
          break;

        case "category":
          this.activeComponent = _admin_Category_vue__WEBPACK_IMPORTED_MODULE_4__["default"];
          this.$router.push({
            name: 'admin-pages',
            params: {
              page: 'category'
            }
          });
          break;

        default:
          this.activeComponent = _admin_Main_vue__WEBPACK_IMPORTED_MODULE_2__["default"];
          this.$router.push({
            name: 'admin'
          })["catch"](function (err) {});
          break;

        case "coupons":
          this.activeComponent = _admin_Coupons_vue__WEBPACK_IMPORTED_MODULE_8__["default"];
          this.$router.push({
            name: 'admin-pages',
            params: {
              page: 'coupons'
            }
          });
          break;
      }
    },
    toggleNavbar: function toggleNavbar() {
      this.show = !this.show;
    },
    logout: function logout() {
      localStorage.removeItem('shop.mdi.in.rs.jwt');
      localStorage.removeItem('shop.mdi.in.rs.user');
      this.user_type = 0;
      this.$router.push('/');
      window.location.reload();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Brand.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Brand.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.js */ "./node_modules/sweetalert2/dist/sweetalert2.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2/src/sweetalert2.scss */ "./node_modules/sweetalert2/src/sweetalert2.scss");
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      //   isLoggedIn : null,
      brands: [],
      brand: {
        id: '',
        brand: ''
      },
      brand_id: '',
      edit: false
    };
  },
  mounted: function mounted() {//     this.isLoggedIn = localStorage.getItem('shop.mdi.in.rs.jwt') != null;
  },
  created: function created() {
    this.allBrands();
  },
  methods: {
    allBrands: function allBrands() {
      var _this = this;

      var uri = '/api/brands';
      this.axios.get(uri).then(function (response) {
        _this.brands = response.data;
      });
    },
    stopEdit: function stopEdit() {
      this.edit = false;
    },
    deleteBrand: function deleteBrand(id) {
      var _this2 = this;

      var swalWithBootstrapButtons = sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      });
      swalWithBootstrapButtons.fire({
        title: 'Da li ste sigurni ?',
        text: "Želite da obrišete ovaj brend!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Da, obriši ga!',
        cancelButtonText: 'Ne!',
        reverseButtons: true
      }).then(function (result) {
        if (result.value) {
          var uri = "/api/brands/delete/".concat(id);

          _this2.axios["delete"](uri, {
            headers: {
              'Access-Control-Allow-Origin': '*'
            }
          }).then(function (response) {
            swalWithBootstrapButtons.fire('Obrisan!', 'Brend je obrisan.', 'success');

            _this2.clearForm();

            _this2.allBrands();
          });
        } else if (
        /* Read more about handling dismissals below */
        result.dismiss === sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.DismissReason.cancel) {
          swalWithBootstrapButtons.fire('Otkazano', 'Brend je i dalje aktivan', 'info');
        }
      });
    },
    addBrand: function addBrand() {
      var _this3 = this;

      if (this.edit === false) {
        var uri = '/api/brands/store';
        this.axios.post(uri, this.brand).then(function (response) {
          _this3.clearForm();

          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Novi brend dodat!',
            showConfirmButton: false,
            timer: 1500
          });

          _this3.allBrands();
        });
      } else {
        // update brands
        var _uri = "/api/brands/update/".concat(this.brand.id);

        this.axios.post(_uri, this.brand).then(function (response) {
          _this3.clearForm();

          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Podaci o brendu su izmenjeni!',
            showConfirmButton: false,
            timer: 1500
          });

          _this3.allBrands();
        });
      }
    },
    editBrand: function editBrand(brand) {
      this.edit = true;
      this.brand.id = brand.id;
      this.brand.brand_id = brand.brand_id;
      this.brand.brand = brand.brand;
    },
    clearForm: function clearForm() {
      this.edit = false;
      this.brand.id = null;
      this.brand.brand_id = null;
      this.brand.brand = null;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Category.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Category.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.js */ "./node_modules/sweetalert2/dist/sweetalert2.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2/src/sweetalert2.scss */ "./node_modules/sweetalert2/src/sweetalert2.scss");
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      //   isLoggedIn : null,
      categories: [],
      category: {
        id: '',
        kategorija: '',
        p_id: ''
      },
      category_id: '',
      edit: false
    };
  },
  mounted: function mounted() {//    this.isLoggedIn = localStorage.getItem('shop.mdi.in.rs.jwt') != null;
  },
  created: function created() {
    this.sveKategorije();
  },
  methods: {
    sveKategorije: function sveKategorije() {
      var _this = this;

      var uri = '/api/categories';
      this.axios.get(uri).then(function (response) {
        _this.categories = response.data;
      });
    },
    stopEdit: function stopEdit() {
      this.edit = false;
    },
    deleteCategory: function deleteCategory(id) {
      var _this2 = this;

      var swalWithBootstrapButtons = sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      });
      swalWithBootstrapButtons.fire({
        title: 'Da li ste sigurni ?',
        text: "Želite da obrišete ovu kategoriju!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Da, obriši je!',
        cancelButtonText: 'Ne!',
        reverseButtons: true
      }).then(function (result) {
        if (result.value) {
          var uri = "/api/category/delete/".concat(id);

          _this2.axios["delete"](uri, {
            headers: {
              'Access-Control-Allow-Origin': '*'
            }
          }).then(function (response) {
            swalWithBootstrapButtons.fire('Obrisan!', 'Vaša kategorija je obrisana.', 'success');

            _this2.clearForm();

            _this2.sveKategorije();
          });
        } else if (
        /* Read more about handling dismissals below */
        result.dismiss === sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.DismissReason.cancel) {
          swalWithBootstrapButtons.fire('Otkazano', 'Vaša kategorija je i dalje aktivna', 'info');
        }
      });
    },
    addCategory: function addCategory() {
      var _this3 = this;

      if (this.edit === false) {
        var uri = '/api/category/create';
        this.axios.post(uri, this.category).then(function (response) {
          _this3.clearForm();

          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Kategorija je dodata!',
            showConfirmButton: false,
            timer: 1500
          });

          _this3.sveKategorije();
        });
      } else {
        // update Category
        var _uri = "/api/category/update/".concat(this.category.id);

        this.axios.post(_uri, this.category).then(function (response) {
          _this3.clearForm();

          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Kategorija je izmenjena!',
            showConfirmButton: false,
            timer: 1500
          });

          _this3.sveKategorije();
        });
      }
    },
    editCategory: function editCategory(category) {
      this.edit = true;
      this.category.id = category.id;
      this.category.category_id = category.category_id;
      this.category.kategorija = category.kategorija;
      this.category.p_id = category.p_id;
    },
    clearForm: function clearForm() {
      this.edit = false;
      this.category.id = null;
      this.category.category_id = null;
      this.category.kategorija = null;
      this.category.p_id = null;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Coupons.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Coupons.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.js */ "./node_modules/sweetalert2/dist/sweetalert2.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2/src/sweetalert2.scss */ "./node_modules/sweetalert2/src/sweetalert2.scss");
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      //   isLoggedIn : null,
      coupons: [],
      coupon: {
        id: '',
        coupon: '',
        valid_until: '',
        amount: '',
        percent: ''
      },
      edit: false
    };
  },
  mounted: function mounted() {//  this.isLoggedIn = localStorage.getItem('shop.mdi.in.rs.jwt') != null;
  },
  created: function created() {
    this.all();
  },
  methods: {
    all: function all() {
      var _this = this;

      var uri = '/api/coupons';
      this.axios.get(uri).then(function (response) {
        _this.coupons = response.data;
      });
    },
    deleteCoupon: function deleteCoupon(id) {
      var _this2 = this;

      var swalWithBootstrapButtons = sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      });
      swalWithBootstrapButtons.fire({
        title: 'Da li ste sigurni ?',
        text: "Želite da obrišete ovaj kupon!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Da, obriši ga!',
        cancelButtonText: 'Ne!',
        reverseButtons: true
      }).then(function (result) {
        if (result.value) {
          var uri = "/api/coupons/delete/".concat(id);

          _this2.axios["delete"](uri, {
            headers: {
              'Access-Control-Allow-Origin': '*'
            }
          }).then(function (response) {
            swalWithBootstrapButtons.fire('Obrisan!', 'Vaš kupon je obrisan.', 'success');

            _this2.clearForm();

            _this2.all();
          });
        } else if (
        /* Read more about handling dismissals below */
        result.dismiss === sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.DismissReason.cancel) {
          swalWithBootstrapButtons.fire('Otkazano', 'Vaš kupon je i dalje aktivan', 'info');
        }
      });
    },
    allCustomers: function allCustomers(id) {
      var _this3 = this;

      var uri = "/api/coupons/all/".concat(id);
      this.axios.get(uri).then(function (response) {
        _this3.clearForm();

        sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Kupon je dodeljen svim registrovanim kupcima!',
          showConfirmButton: false,
          timer: 1500
        });

        _this3.all();
      });
    },
    regularCustomers: function regularCustomers(id) {
      var _this4 = this;

      var uri = "/api/coupons/regular/".concat(id);
      this.axios.get(uri).then(function (response) {
        _this4.clearForm();

        sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Kupon je dodeljen redovnim kupcima!',
          showConfirmButton: false,
          timer: 1500
        });

        _this4.all();
      });
    },
    add: function add() {
      var _this5 = this;

      if (this.edit === false) {
        var uri = '/api/coupons/create';
        this.axios.post(uri, this.coupon).then(function (response) {
          _this5.clearForm();

          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Novi kupon je dodat!',
            showConfirmButton: false,
            timer: 1500
          });

          _this5.all();
        });
      } else {
        // update 
        var _uri = "/api/coupons/update/".concat(this.coupon.id);

        this.axios.post(_uri, this.coupon).then(function (response) {
          _this5.clearForm();

          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Podaci o kuponu su izmenjeni!',
            showConfirmButton: false,
            timer: 1500
          });

          _this5.all();
        });
      }
    },
    editCoupon: function editCoupon(coupon) {
      this.edit = true;
      this.coupon.id = coupon.id;
      this.coupon.coupon = coupon.coupon;
      this.coupon.valid_until = coupon.valid_until;
      this.coupon.amount = coupon.amount;
      this.coupon.percent = coupon.percent;
    },
    stopEdit: function stopEdit() {
      this.edit = false;
    },
    clearForm: function clearForm() {
      this.edit = false;
      this.coupon.id = null;
      this.coupon.coupon = null;
      this.coupon.valid_until = null;
      this.coupon.amount = null;
      this.coupon.percent = null;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Main.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      user: null,
      orders: [],
      products: [],
      users: [],
      categories: [],
      brands: []
    };
  },
  mounted: function mounted() {
    var _this = this;

    axios.get('/api/users/').then(function (response) {
      return _this.users = response.data;
    });
    axios.get('/api/categories/').then(function (response) {
      _this.categories = response.data;
    });
    axios.get('/api/orders/').then(function (response) {
      return _this.orders = response.data;
    });
    axios.get('/api/products/').then(function (response) {
      _this.products = response.data.product;
      _this.brands = response.data.brands;
      _this.categories = response.data.cat;
    });
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Orders.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Orders.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.js */ "./node_modules/sweetalert2/dist/sweetalert2.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2/src/sweetalert2.scss */ "./node_modules/sweetalert2/src/sweetalert2.scss");
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      edit: false,
      orders: [],
      order: {
        id: '',
        firstname: '',
        lastname: '',
        country: '',
        streetaddress: '',
        is_delivered: '',
        postcode: '',
        town: '',
        phone: '',
        email: '',
        orders: []
      }
    };
  },
  beforeMount: function beforeMount() {
    this.start();
  },
  computed: {
    total: function total() {
      return this.order.orders.reduce(function (acc, item) {
        if (item.is_delivered != 1) {
          if (item.discount_price) {
            return acc + item.discount_price * item.quantity;
          } else {
            return acc + item.price * item.quantity;
          }
        } else {
          return acc + 0;
        }
      }, 0);
    },
    delivery: function delivery() {
      return this.order.orders.reduce(function (acc, item) {
        if (item.is_delivered != 1) {
          return acc + 1;
        } else {
          return acc + 0;
        }
      }, 0);
    }
  },
  methods: {
    start: function start() {
      var _this = this;

      axios.get('/api/orders/').then(function (response) {
        return _this.orders = response.data;
      });
    },
    clear: function clear() {
      this.edit = false;
      this.order.id = null;
      this.order.firstname = null;
      this.order.lastname = null;
      this.order.country = null;
      this.order.streetaddress = null;
      this.order.postcode = null;
      this.order.town = null;
      this.order.phone = null;
      this.order.email = null;
      this.order.orders = null;
    },
    editOrder: function editOrder(order) {
      this.edit = true;
      this.order.id = order.id;
      this.order.firstname = order.firstname;
      this.order.lastname = order.lastname;
      this.order.country = order.country;
      this.order.streetaddress = order.streetaddress;
      this.order.postcode = order.postcode;
      this.order.town = order.town;
      this.order.phone = order.phone;
      this.order.email = order.email;
      this.order.orders = order.orders;
    },
    deliver: function deliver(id) {
      var _this2 = this;

      axios.patch("/api/orders/".concat(id, "/deliver")).then(function (response) {
        _this2.start();

        _this2.clear();

        sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
          position: 'top-end',
          icon: 'warning',
          title: 'Items are delivered!',
          showConfirmButton: false,
          timer: 1500
        });
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/ProductModal.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/ProductModal.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['product'],
  data: function data() {
    return {
      attachment: null
    };
  },
  computed: {
    data: function data() {
      if (this.product != null) {
        return this.product;
      }

      return {
        name: "",
        units: "",
        price: "",
        description: "",
        image: false
      };
    }
  },
  methods: {
    attachFile: function attachFile(event) {
      this.attachment = event.target.files[0];
    },
    uploadFile: function uploadFile(event) {
      var _this = this;

      if (this.attachment != null) {
        var formData = new FormData();
        formData.append("image", this.attachment);
        var headers = {
          'Content-Type': 'multipart/form-data'
        };
        axios.post("/api/upload-file", formData, {
          headers: headers
        }).then(function (response) {
          _this.product.image = response.data;

          _this.$emit('close', _this.product);
        });
      } else {
        this.$emit('close', this.product);
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Products.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Products.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProductModal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProductModal */ "./resources/js/components/admin/ProductModal.vue");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.js */ "./node_modules/sweetalert2/dist/sweetalert2.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2/src/sweetalert2.scss */ "./node_modules/sweetalert2/src/sweetalert2.scss");
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_2__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      image_size: {
        width: 200,
        height: 200,
        "class": 'm1'
      },
      image_src: '/uploads/products/',
      image_update: false,
      category_id: '',
      products: [],
      categories: [],
      brands: [],
      tags: [],
      product: {
        id: '',
        name: '',
        description: '',
        category_id: null,
        brand_id: null,
        tag_id: null,
        size: null,
        color: null,
        units: '',
        price: '',
        //  discount_price: '',
        image: ''
      },
      category: {
        id: '',
        kategorija: ''
      },
      tag: {
        id: '',
        tag: ''
      },
      brand: {
        id: '',
        brand: ''
      },
      editingItem: null,
      addingProduct: null,
      fields: [{
        key: 'name',
        label: 'Name',
        sortable: true,
        sortDirection: 'asc'
      }, {
        key: 'units',
        label: 'Units',
        sortable: true,
        sortDirection: 'asc'
      }, {
        key: 'price',
        label: 'Price',
        sortable: true,
        sortDirection: 'asc'
      }, //  { key: 'description', label: 'Description', sortable: true, sortDirection: 'asc' },
      {
        key: 'category_id',
        label: 'Kategorija',
        sortable: true,
        sortDirection: 'asc'
      }, {
        key: 'brand_id',
        label: 'Brend',
        sortable: true,
        sortDirection: 'asc'
      }, {
        key: 'tag_id',
        label: 'Brend',
        sortable: true,
        sortDirection: 'asc'
      }, {
        key: 'size',
        label: 'Size',
        sortable: true,
        sortDirection: 'asc'
      }, {
        key: 'color',
        label: 'Color',
        sortable: true,
        sortDirection: 'asc'
      }, 'action'],
      form: false,
      product_id: '',
      pocetak: true,
      edit: false,
      totalRows: 1,
      currentPage: 1,
      perPage: 5,
      pageOptions: [5, 10, 15],
      sortBy: '',
      sortDesc: false,
      sortDirection: 'asc',
      filter: null,
      filterOn: []
    };
  },
  components: {
    Modal: _ProductModal__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  beforeMount: function beforeMount() {
    this.start();
  },
  filters: {
    truncate: function truncate(content, length, suffix) {
      return content.substring(0, length) + suffix;
    }
  },
  computed: {
    sortOptions: function sortOptions() {
      // Create an options list from our fields
      return this.fields.filter(function (f) {
        return f.sortable;
      }).map(function (f) {
        return {
          text: f.label,
          value: f.key
        };
      });
    }
  },
  mounted: function mounted() {
    // Set the initial number of items    
    this.totalRows = this.products.length;
  },
  methods: {
    start: function start() {
      var _this = this;

      axios.get('/api/products/').then(function (response) {
        _this.products = response.data.product;
        _this.tags = response.data.tags;
        _this.brands = response.data.brands;
        _this.categories = response.data.cat;
        _this.totalRows = _this.products.length;
      });
    },
    showModal: function showModal() {
      this.$refs['my-modal'].show();
    },
    hideModal: function hideModal() {
      this.edit = false;
      this.$refs['my-modal'].hide();
    },
    toggleModal: function toggleModal() {
      // We pass the ID of the button that we want to return focus to
      // when the modal has hidden
      this.$refs['my-modal'].toggle('#toggle-btn');
    },
    fieldChange: function fieldChange(e) {
      var _this2 = this;

      var selectedFile = new FileReader();
      selectedFile.readAsDataURL(e.target.files[0]);

      selectedFile.onload = function (e) {
        _this2.product.image = e.target.result;
        _this2.image_update = true;
      };

      console.log(this.product.image);
    },
    newProduct: function newProduct() {
      this.addingProduct = {
        name: null,
        units: null,
        price: null,
        image: null,
        description: null,
        color: null,
        size: null,
        brand_id: null,
        tag_id: null
      };
    },
    endEditing: function endEditing(product) {
      var _this3 = this;

      this.editingItem = null;
      this.edit = false;
      var index = this.products.indexOf(product);
      var name = product.name;
      var units = product.units;
      var price = product.price;
      var description = product.description;
      axios.put("/api/products/".concat(product.id), {
        name: name,
        units: units,
        price: price,
        description: description
      }).then(function (response) {
        return _this3.products[index] = product;
      });
    },
    addProduct: function addProduct() {
      var _this4 = this;

      if (this.edit === false) {
        this.addingProduct = null;
        var name = this.product.name;
        var units = this.product.units;
        var price = this.product.price; // let discount_price = this.product.discount_price

        var description = this.product.description;
        var image = this.product.image;
        var category_id = this.product.category_id;
        var brand_id = this.product.brand_id;
        var tag_id = this.product.tag_id;
        var size = this.product.size;
        var color = this.product.color;
        axios.post('/api/products/create', {
          name: name,
          units: units,
          price: price,
          description: description,
          image: image,
          category_id: category_id,
          brand_id: brand_id,
          tag_id: tag_id,
          color: color,
          size: size
        }).then(function (response) {
          _this4.start();

          _this4.clearForm();

          _this4.totalRows = _this4.products.length;
          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Novi proizvod je unet!',
            showConfirmButton: false,
            timer: 1500
          });
        });
      } else {
        var uri = "/api/products/update/".concat(this.product.id);
        this.axios.post(uri, this.product).then(function (response) {
          _this4.start();

          _this4.clearForm();

          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Podaci o proizvodu su promenjeni!',
            showConfirmButton: false,
            timer: 1500
          });
        });
      }
    },
    editProduct: function editProduct(product) {
      this.edit = true;
      this.showModal();
      this.product.id = product.id;
      this.product.name = product.name;
      this.product.units = product.units;
      this.product.price = product.price; // this.product.discount_price = product.discount_price

      this.product.image = product.image;
      this.product.description = product.description;
      this.product.category_id = product.category_id;
      this.product.brand_id = product.brand_id;
      this.product.tag_id = product.tag_id;
      this.product.color = product.color;
      this.product.size = product.size;
    },
    clearForm: function clearForm() {
      this.edit = false;
      this.$refs['my-modal'].hide();
      this.product.id = null;
      this.product.name = null;
      this.product.units = null;
      this.product.price = null; //  this.product.discount_price = null

      this.product.image = null;
      this.product.description = null;
      this.product.category_id = null;
      this.product.brand_id = null;
      this.product.tag_id = null;
      this.product.color = null;
      this.product.size = null;
    },
    deleteProduct: function deleteProduct(id) {
      var _this5 = this;

      var swalWithBootstrapButtons = sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      });
      swalWithBootstrapButtons.fire({
        title: 'Da li ste sigurni ?',
        text: "Želite da obrišete ovaj proizvod!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Da, obriši ga!',
        cancelButtonText: 'Ne, otkaži!',
        reverseButtons: true
      }).then(function (result) {
        if (result.value) {
          var uri = "/api/products/delete/".concat(id);

          _this5.axios["delete"](uri).then(function (response) {
            swalWithBootstrapButtons.fire('Obrisan!', 'Vaš proizvod je obrisan.', 'success');

            _this5.start();
          });
        } else if (
        /* Read more about handling dismissals below */
        result.dismiss === sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_1___default.a.DismissReason.cancel) {
          swalWithBootstrapButtons.fire('Otkazano', 'Vaš proizvod je i dalje u ponudi', 'info');
        }
      });
    },
    stopEdit: function stopEdit() {
      this.edit = false;
      this.$refs['my-modal'].hide();
    },
    onFiltered: function onFiltered(filteredItems) {
      // Trigger pagination to update the number of buttons/pages due to filtering
      this.totalRows = filteredItems.length;
      this.currentPage = 1;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Tag.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Tag.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.js */ "./node_modules/sweetalert2/dist/sweetalert2.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2/src/sweetalert2.scss */ "./node_modules/sweetalert2/src/sweetalert2.scss");
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      //     isLoggedIn : null,
      tags: [],
      tag: {
        id: '',
        tag: ''
      },
      tag_id: '',
      edit: false
    };
  },
  mounted: function mounted() {//     this.isLoggedIn = localStorage.getItem('shop.in.rs.jwt') != null;
  },
  created: function created() {
    this.all();
  },
  methods: {
    stopEdit: function stopEdit() {
      this.edit = false;
    },
    all: function all() {
      var _this = this;

      var uri = '/api/tags';
      this.axios.get(uri).then(function (response) {
        _this.tags = response.data;
      });
    },
    deleteTag: function deleteTag(id) {
      var _this2 = this;

      var swalWithBootstrapButtons = sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      });
      swalWithBootstrapButtons.fire({
        title: 'Da li ste sigurni ?',
        text: "Želite da obrišete ovaj tag!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Da, obriši ga!',
        cancelButtonText: 'Ne!',
        reverseButtons: true
      }).then(function (result) {
        if (result.value) {
          var uri = "/api/tags/delete/".concat(id);

          _this2.axios["delete"](uri, {
            headers: {
              'Access-Control-Allow-Origin': '*'
            }
          }).then(function (response) {
            swalWithBootstrapButtons.fire('Obrisan!', 'Vaš tag je obrisan.', 'success');

            _this2.clearForm();

            _this2.all();
          });
        } else if (
        /* Read more about handling dismissals below */
        result.dismiss === sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.DismissReason.cancel) {
          swalWithBootstrapButtons.fire('Otkazano', 'Vaš tag je i dalje aktivan', 'info');
        }
      });
    },
    add: function add() {
      var _this3 = this;

      if (this.edit === false) {
        var uri = '/api/tags/create';
        this.axios.post(uri, this.tag).then(function (response) {
          _this3.clearForm();

          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Novi tag je dodat!',
            showConfirmButton: false,
            timer: 1500
          });

          _this3.all();
        });
      } else {
        // update 
        var _uri = "/api/tags/update/".concat(this.tag.id);

        this.axios.post(_uri, this.tag).then(function (response) {
          _this3.clearForm();

          sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Podaci o tagu su izmenjeni!',
            showConfirmButton: false,
            timer: 1500
          });

          _this3.all();
        });
      }
    },
    editTag: function editTag(tag) {
      this.edit = true;
      this.tag.id = tag.id;
      this.tag.tag_id = tag.tag_id;
      this.tag.tag = tag.tag;
    },
    clearForm: function clearForm() {
      this.edit = false;
      this.tag.id = null;
      this.tag.tag_id = null;
      this.tag.tag = null;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Users.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Users.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.js */ "./node_modules/sweetalert2/dist/sweetalert2.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2/src/sweetalert2.scss */ "./node_modules/sweetalert2/src/sweetalert2.scss");
/* harmony import */ var sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_src_sweetalert2_scss__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      rola: '',
      users: [],
      user: {
        id: '',
        name: '',
        email: '',
        created_at: '',
        is_admin: ''
      },
      proba: {
        name: ''
      }
    };
  },
  created: function created() {
    this.useri();
  },
  methods: {
    useri: function useri() {
      var _this = this;

      axios.get('/api/users/').then(function (response) {
        return _this.users = response.data;
      });
    },
    change: function change(e) {
      var _this2 = this;

      // Promena role
      var id = e.id;
      var rola = e.is_admin;
      var uri = '/api/users/rola';
      this.axios.post(uri, {
        id: id,
        rola: rola
      }).then(function (response) {
        _this2.useri();

        sweetalert2_dist_sweetalert2_js__WEBPACK_IMPORTED_MODULE_0___default.a.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Rola je promenjena!',
          showConfirmButton: false,
          timer: 1500
        });
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Admin.vue?vue&type=style&index=0&scope=true&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Admin.vue?vue&type=style&index=0&scope=true&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.hero-section {\n\t\theight: 20vh;\n\t\tbackground: #ababab;\n\t\talign-items: center;\n\t\tmargin-bottom: 20px;\n\t\tmargin-top: -20px;\n}\n.title {\n\t\tfont-size: 60px;\n\t\tcolor: #ffffff;\n}\nbutton.navbar-toggler{\n \n\tmargin-top: 0px;\n}\n.p {\n\t\tmargin-top: 20%;\n}\n.fixed-top-2 {\n\tmargin-top: 80px;\n}\n.b-primary {\n\t\twidth: 120px;\n}\n.na {\n\tbackground-color: #FC611F !important;\n}\n@media (min-width: 768px) {\n.nav {\n    display: none;\n}\n}\n.sticky {\n\tlist-style-type: none;\n\tposition: -webkit-sticky;\n\tposition: sticky;\n\toverflow: hidden;\n\ttop: 82px;\n\twidth: 100%;\n}\na {\n    color: #E7AB3C !important;\n}\n\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=style&index=0&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Main.vue?vue&type=style&index=0&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\na, a:hover {\n\tcolor: #1E4356 !important;\n\ttext-decoration: none;\n}\n.main {\n    margin-top: 50px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.big-text[data-v-137c6ca4] {\n\tfont-size: 28px;\n}\n.product-box[data-v-137c6ca4] {\n\tborder: 1px solid #cccccc;\n\tpadding: 10px 15px;\n\theight: 20vh\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/ProductModal.vue?vue&type=style&index=0&id=3988972e&scoped=true&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/ProductModal.vue?vue&type=style&index=0&id=3988972e&scoped=true&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.modal-mask[data-v-3988972e] {\n    position: fixed;\n    z-index: 9998;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    background-color: rgba(0, 0, 0, .5);\n    display: table;\n    transition: opacity .3s ease;\n}\n.modal-wrapper[data-v-3988972e] {\n    display: table-cell;\n    vertical-align: middle;\n}\n.modal-container[data-v-3988972e] {\n    width: 300px;\n    margin: 0px auto;\n    padding: 20px 30px;\n    background-color: #fff;\n    border-radius: 2px;\n    box-shadow: 0 2px 8px rgba(0, 0, 0, .33);\n    transition: all .3s ease;\n    font-family: Helvetica, Arial, sans-serif;\n}\n.modal-header h3[data-v-3988972e] {\n    margin-top: 0;\n    color: #42b983;\n}\n.modal-body[data-v-3988972e] {\n    margin: 20px 0;\n}\n.modal-default-button[data-v-3988972e] {\n    float: right;\n}\n.modal-enter[data-v-3988972e] {\n    opacity: 0;\n}\n.modal-leave-active[data-v-3988972e] {\n    opacity: 0;\n}\n.modal-enter .modal-container[data-v-3988972e],\n    .modal-leave-active .modal-container[data-v-3988972e] {\n        transform: scale(1.1);\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Products.vue?vue&type=style&index=0&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Products.vue?vue&type=style&index=0&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.btn-danger {\n    margin-right: 3px !important;\n}\n\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Admin.vue?vue&type=style&index=0&scope=true&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Admin.vue?vue&type=style&index=0&scope=true&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./Admin.vue?vue&type=style&index=0&scope=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Admin.vue?vue&type=style&index=0&scope=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=style&index=0&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Main.vue?vue&type=style&index=0&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Main.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/ProductModal.vue?vue&type=style&index=0&id=3988972e&scoped=true&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/ProductModal.vue?vue&type=style&index=0&id=3988972e&scoped=true&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductModal.vue?vue&type=style&index=0&id=3988972e&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/ProductModal.vue?vue&type=style&index=0&id=3988972e&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Products.vue?vue&type=style&index=0&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Products.vue?vue&type=style&index=0&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Products.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Products.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Admin.vue?vue&type=template&id=58b78718&":
/*!********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Admin.vue?vue&type=template&id=58b78718& ***!
  \********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { attrs: { id: "rout" } }, [
    _vm.user_type == 1
      ? _c("header", { staticClass: "header-section " }, [
          _c("div", { staticClass: "nav-item nav1" }, [
            _c("nav", { staticClass: "nav-menu mobile-menu" }, [
              _c("ul", [
                _c("li", { staticClass: "nav-link" }, [
                  _c(
                    "a",
                    {
                      on: {
                        click: function($event) {
                          return _vm.setComponent("main")
                        }
                      }
                    },
                    [_vm._v("Dashboard")]
                  )
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "nav-link" }, [
                  _c(
                    "a",
                    {
                      on: {
                        click: function($event) {
                          return _vm.setComponent("orders")
                        }
                      }
                    },
                    [_vm._v("Orders")]
                  )
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "nav-link" }, [
                  _c(
                    "a",
                    {
                      on: {
                        click: function($event) {
                          return _vm.setComponent("products")
                        }
                      }
                    },
                    [_vm._v("Products")]
                  )
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "nav-link" }, [
                  _c(
                    "a",
                    {
                      on: {
                        click: function($event) {
                          return _vm.setComponent("users")
                        }
                      }
                    },
                    [_vm._v("Users")]
                  )
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "nav-link" }, [
                  _c(
                    "a",
                    {
                      on: {
                        click: function($event) {
                          return _vm.setComponent("category")
                        }
                      }
                    },
                    [_vm._v("Category")]
                  )
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "nav-link" }, [
                  _c(
                    "a",
                    {
                      on: {
                        click: function($event) {
                          return _vm.setComponent("brand")
                        }
                      }
                    },
                    [_vm._v("Brands")]
                  )
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "nav-link" }, [
                  _c(
                    "a",
                    {
                      on: {
                        click: function($event) {
                          return _vm.setComponent("tag")
                        }
                      }
                    },
                    [_vm._v("Tags")]
                  )
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "nav-link" }, [
                  _c(
                    "a",
                    {
                      on: {
                        click: function($event) {
                          return _vm.setComponent("coupons")
                        }
                      }
                    },
                    [_vm._v("Coupons")]
                  )
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "nav-link" }, [
                  _c("a", { attrs: { href: "" } }, [
                    _vm._v(" Hi, " + _vm._s(_vm.userdata.firstname))
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "li",
                  { staticClass: "nav-link", on: { click: _vm.logout } },
                  [_c("a", { attrs: { href: "" } }, [_vm._v(" Logout")])]
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c(
            "nav",
            {
              staticClass:
                "navbar navbar-dark navbar-expand-md  nav nav-menu text-center",
              class: { navbarOpen: _vm.show }
            },
            [
              _c(
                "button",
                {
                  staticClass: "navbar-toggler ml-auto",
                  attrs: {
                    type: "button",
                    "data-toggle": "collapse",
                    "data-target": "#collapsibleNavbar",
                    "aria-expanded": "false"
                  },
                  on: {
                    click: function($event) {
                      $event.stopPropagation()
                      return _vm.toggleNavbar()
                    }
                  }
                },
                [
                  _vm._v("\n                    MENU "),
                  _c("span", { staticClass: "navbar-toggler-icon" })
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "collapse navbar-collapse mr-4",
                  class: { show: _vm.show },
                  attrs: { id: "collapsibleNavbar" }
                },
                [
                  _c("ul", { staticClass: "navbar-nav ml-auto mx-auto" }, [
                    _c("li", { staticClass: "nav-link" }, [
                      _c(
                        "a",
                        {
                          on: {
                            click: function($event) {
                              return _vm.setComponent("main")
                            }
                          }
                        },
                        [_vm._v("Dashboard")]
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", { staticClass: "nav-link" }, [
                      _c(
                        "a",
                        {
                          on: {
                            click: function($event) {
                              return _vm.setComponent("orders")
                            }
                          }
                        },
                        [_vm._v("Orders")]
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", { staticClass: "nav-link" }, [
                      _c(
                        "a",
                        {
                          on: {
                            click: function($event) {
                              return _vm.setComponent("products")
                            }
                          }
                        },
                        [_vm._v("Products")]
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", { staticClass: "nav-link" }, [
                      _c(
                        "a",
                        {
                          on: {
                            click: function($event) {
                              return _vm.setComponent("users")
                            }
                          }
                        },
                        [_vm._v("Users")]
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", { staticClass: "nav-link" }, [
                      _c(
                        "a",
                        {
                          on: {
                            click: function($event) {
                              return _vm.setComponent("category")
                            }
                          }
                        },
                        [_vm._v("Category")]
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", { staticClass: "nav-link" }, [
                      _c(
                        "a",
                        {
                          on: {
                            click: function($event) {
                              return _vm.setComponent("brand")
                            }
                          }
                        },
                        [_vm._v("Brands")]
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", { staticClass: "nav-link" }, [
                      _c(
                        "a",
                        {
                          on: {
                            click: function($event) {
                              return _vm.setComponent("tag")
                            }
                          }
                        },
                        [_vm._v("Tags")]
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", { staticClass: "nav-link" }, [
                      _c(
                        "a",
                        {
                          on: {
                            click: function($event) {
                              return _vm.setComponent("coupons")
                            }
                          }
                        },
                        [_vm._v("Coupons")]
                      )
                    ]),
                    _vm._v(" "),
                    _c("li", { staticClass: "nav-link" }, [
                      _c("a", { attrs: { href: "" } }, [
                        _vm._v(" Hi, " + _vm._s(_vm.userdata.firstname))
                      ])
                    ]),
                    _vm._v(" "),
                    _c(
                      "li",
                      { staticClass: "nav-link", on: { click: _vm.logout } },
                      [_c("a", { attrs: { href: "" } }, [_vm._v(" Logout")])]
                    )
                  ])
                ]
              )
            ]
          )
        ])
      : _vm._e(),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "container", attrs: { id: "rout" } },
      [_c(_vm.activeComponent, { tag: "component" })],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Brand.vue?vue&type=template&id=3ebac42c&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Brand.vue?vue&type=template&id=3ebac42c& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container", attrs: { id: "rout" } }, [
    _c("br"),
    _c("br"),
    _c("br"),
    _vm._v(" "),
    _c("div", [
      _vm.edit
        ? _c("div", [_c("h1", [_vm._v("Izmeni podatke o brendu:")])])
        : _c("div", [_c("h1", [_vm._v("Dodaj brend:")])]),
      _vm._v(" "),
      _c(
        "form",
        {
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.addBrand($event)
            }
          }
        },
        [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.brand.brand,
                      expression: "brand.brand"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text" },
                  domProps: { value: _vm.brand.brand },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.brand, "brand", $event.target.value)
                    }
                  }
                })
              ])
            ])
          ]),
          _vm._v(" "),
          _c("br"),
          _vm._v(" "),
          _vm.edit
            ? _c("div", { staticClass: "form-group" }, [
                _c("button", { staticClass: "site-btn place-btn" }, [
                  _vm._v("Izmeni")
                ]),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "site-btn place-btn",
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        return _vm.clearForm()
                      }
                    }
                  },
                  [_vm._v("Povratak na prethodnu stranu")]
                )
              ])
            : _c("div", { staticClass: "form-group" }, [
                _c("button", { staticClass: "site-btn place-btn" }, [
                  _vm._v("Kreiraj")
                ])
              ])
        ]
      )
    ]),
    _vm._v(" "),
    _c("table", { staticClass: "table table-hover" }, [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "tbody",
        _vm._l(_vm.brands, function(brand) {
          return _c("tr", { key: brand.id }, [
            _c("td", [_vm._v(_vm._s(brand.id))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(brand.brand))]),
            _vm._v(" "),
            _c("td", [
              _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.editBrand(brand)
                    }
                  }
                },
                [_vm._v("Edit")]
              ),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.deleteBrand(brand.id)
                    }
                  }
                },
                [_vm._v("Delete")]
              )
            ])
          ])
        }),
        0
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("ID")]),
        _vm._v(" "),
        _c("th", [_vm._v("Brend")]),
        _vm._v(" "),
        _c("th", [_vm._v("Actions")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Category.vue?vue&type=template&id=d1b3502e&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Category.vue?vue&type=template&id=d1b3502e& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container", attrs: { id: "rout" } }, [
    _c("br"),
    _c("br"),
    _c("br"),
    _vm._v(" "),
    _c("div", [
      _vm.edit
        ? _c("div", [_c("h1", [_vm._v("Izmeni Kategoriju:")])])
        : _c("div", [_c("h1", [_vm._v("Dodaj Kategoriju:")])]),
      _vm._v(" "),
      _c(
        "form",
        {
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.addCategory($event)
            }
          }
        },
        [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.category.kategorija,
                      expression: "category.kategorija"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text" },
                  domProps: { value: _vm.category.kategorija },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.category, "kategorija", $event.target.value)
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6" }, [
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _c(
                    "b-form-select",
                    {
                      model: {
                        value: _vm.category.p_id,
                        callback: function($$v) {
                          _vm.$set(_vm.category, "p_id", $$v)
                        },
                        expression: "category.p_id"
                      }
                    },
                    _vm._l(_vm.categories, function(category) {
                      return category.p_id == 0
                        ? _c("option", { domProps: { value: category.id } }, [
                            _vm._v(
                              "\n\n                                    " +
                                _vm._s(category.kategorija) +
                                "\n\n                                "
                            )
                          ])
                        : _vm._e()
                    }),
                    0
                  )
                ],
                1
              )
            ])
          ]),
          _vm._v(" "),
          _c("br"),
          _vm._v(" "),
          _vm.edit
            ? _c("div", { staticClass: "form-group" }, [
                _c("button", { staticClass: "site-btn place-btn" }, [
                  _vm._v("Izmeni")
                ]),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "site-btn place-btn",
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        return _vm.clearForm()
                      }
                    }
                  },
                  [_vm._v("Povratak na prethodnu stranu")]
                )
              ])
            : _c("div", { staticClass: "form-group" }, [
                _c("button", { staticClass: "site-btn place-btn" }, [
                  _vm._v("Kreiraj")
                ])
              ])
        ]
      )
    ]),
    _vm._v(" "),
    _c("table", { staticClass: "table table-hover" }, [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "tbody",
        _vm._l(_vm.categories, function(category) {
          return _c("tr", { key: category.id }, [
            _c("td", [_vm._v(_vm._s(category.id))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(category.kategorija))]),
            _vm._v(" "),
            _c("td", [
              category.p_id == 0 ? _c("div") : _vm._e(),
              _vm._v(" "),
              category.p_id != 0
                ? _c(
                    "div",
                    _vm._l(_vm.categories, function(c) {
                      return _c("div", { key: c.id }, [
                        c.id == category.p_id
                          ? _c("div", [
                              _vm._v(
                                "\n                                    " +
                                  _vm._s(c.kategorija) +
                                  "\n                                "
                              )
                            ])
                          : _vm._e()
                      ])
                    }),
                    0
                  )
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("td", [
              _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.editCategory(category)
                    }
                  }
                },
                [_vm._v("Edit")]
              ),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.deleteCategory(category.id)
                    }
                  }
                },
                [_vm._v("Delete")]
              )
            ])
          ])
        }),
        0
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("ID")]),
        _vm._v(" "),
        _c("th", [_vm._v("Kategorija")]),
        _vm._v(" "),
        _c("th", [_vm._v("Kategorija Roditelj")]),
        _vm._v(" "),
        _c("th", [_vm._v("Actions")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Coupons.vue?vue&type=template&id=4b0e4372&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Coupons.vue?vue&type=template&id=4b0e4372& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container", attrs: { id: "rout" } }, [
    _c("br"),
    _c("br"),
    _c("br"),
    _vm._v(" "),
    _c("div", [
      _vm.edit
        ? _c("div", [_c("h1", [_vm._v("Update coupon:")])])
        : _c("div", [_c("h1", [_vm._v("Add Coupon:")])]),
      _vm._v(" "),
      _c(
        "form",
        {
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.add($event)
            }
          }
        },
        [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c("div", { staticClass: "form-group" }, [
                _vm._v("\n                            Name: "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.coupon.coupon,
                      expression: "coupon.coupon"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text" },
                  domProps: { value: _vm.coupon.coupon },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.coupon, "coupon", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _vm._v("\n                            Amount: "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.coupon.amount,
                      expression: "coupon.amount"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "number" },
                  domProps: { value: _vm.coupon.amount },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.coupon, "amount", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _vm._v("\n                            Percent: "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.coupon.percent,
                      expression: "coupon.percent"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "number" },
                  domProps: { value: _vm.coupon.percent },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.coupon, "percent", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c(
                "div",
                [
                  _c("label", { attrs: { for: "example-datepicker" } }, [
                    _vm._v("Valid Until:")
                  ]),
                  _vm._v(" "),
                  _c("b-form-datepicker", {
                    staticClass: "mb-2",
                    attrs: {
                      id: "example-datepicker",
                      placeholder: "Choose a date",
                      local: "en"
                    },
                    model: {
                      value: _vm.coupon.valid_until,
                      callback: function($$v) {
                        _vm.$set(_vm.coupon, "valid_until", $$v)
                      },
                      expression: "coupon.valid_until"
                    }
                  }),
                  _vm._v(" "),
                  _vm.edit
                    ? _c("p", { staticClass: "mb-1" }, [
                        _vm._v(
                          "Valid Until: '" +
                            _vm._s(_vm.coupon.valid_until) +
                            "'"
                        )
                      ])
                    : _vm._e()
                ],
                1
              )
            ])
          ]),
          _vm._v(" "),
          _c("br"),
          _vm._v(" "),
          _vm.edit
            ? _c("div", { staticClass: "form-group" }, [
                _c("button", { staticClass: "site-btn place-btn" }, [
                  _vm._v("Izmeni")
                ]),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "site-btn place-btn",
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        return _vm.clearForm()
                      }
                    }
                  },
                  [_vm._v("Povratak na prethodnu stranu")]
                )
              ])
            : _c("div", { staticClass: "form-group" }, [
                _c("button", { staticClass: "site-btn place-btn" }, [
                  _vm._v("Kreiraj")
                ])
              ])
        ]
      )
    ]),
    _vm._v(" "),
    _c("table", { staticClass: "table table-hover" }, [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "tbody",
        _vm._l(_vm.coupons, function(coupon) {
          return _c("tr", { key: coupon.id }, [
            _c("td", [_vm._v(_vm._s(coupon.id))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(coupon.coupon))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(coupon.valid_until))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(coupon.amount))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(coupon.percent))]),
            _vm._v(" "),
            _c("td", [
              _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.editCoupon(coupon)
                    }
                  }
                },
                [_vm._v("Edit")]
              ),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.deleteCoupon(coupon.id)
                    }
                  }
                },
                [_vm._v("Delete")]
              )
            ]),
            _vm._v(" "),
            _c("td", [
              _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.allCustomers(coupon.id)
                    }
                  }
                },
                [_vm._v("Assign")]
              )
            ]),
            _vm._v(" "),
            _c("td", [
              _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.regularCustomers(coupon.id)
                    }
                  }
                },
                [_vm._v("Assign")]
              )
            ])
          ])
        }),
        0
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("ID")]),
        _vm._v(" "),
        _c("th", [_vm._v("Coupon")]),
        _vm._v(" "),
        _c("th", [_vm._v("Valid Until")]),
        _vm._v(" "),
        _c("th", [_vm._v("Amount")]),
        _vm._v(" "),
        _c("th", [_vm._v("Percent")]),
        _vm._v(" "),
        _c("th", [_vm._v("Actions")]),
        _vm._v(" "),
        _c("th", [_vm._v("Assign to all customers")]),
        _vm._v(" "),
        _c("th", [_vm._v("Assign to regular customers")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=template&id=137c6ca4&scoped=true&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Main.vue?vue&type=template&id=137c6ca4&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container", attrs: { id: "rout" } }, [
    _c("div", { staticClass: "main row" }, [
      _c(
        "div",
        {
          staticClass:
            "col-md-4 product-box d-flex align-content-center justify-content-center flex-wrap big-text"
        },
        [
          _c("a", { attrs: { href: "/admin/orders" } }, [
            _vm._v("Orders (" + _vm._s(_vm.orders.length) + ")")
          ])
        ]
      ),
      _vm._v(" "),
      _c("hr"),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass:
            "col-md-4 product-box d-flex align-content-center justify-content-center flex-wrap big-text"
        },
        [
          _c("a", { attrs: { href: "/admin/products" } }, [
            _vm._v("Products (" + _vm._s(_vm.products.length) + ")")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass:
            "col-md-4 product-box d-flex align-content-center justify-content-center flex-wrap big-text"
        },
        [
          _c("a", { attrs: { href: "/admin/users" } }, [
            _vm._v("Users (" + _vm._s(_vm.users.length) + ")")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass:
            "col-md-4 product-box d-flex align-content-center justify-content-center flex-wrap big-text"
        },
        [
          _c("a", { attrs: { href: "/admin/category" } }, [
            _vm._v("Categories (" + _vm._s(_vm.categories.length) + ")")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass:
            "col-md-4 product-box d-flex align-content-center justify-content-center flex-wrap big-text"
        },
        [
          _c("a", { attrs: { href: "/admin/brands" } }, [
            _vm._v("Brands (" + _vm._s(_vm.brands.length) + ")")
          ])
        ]
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Orders.vue?vue&type=template&id=77736b10&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Orders.vue?vue&type=template&id=77736b10& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { attrs: { id: "rout" } }, [
    _c("br"),
    _c("br"),
    _c("br"),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-md-6" }, [
        _c("h2", { staticClass: "text-centar" }, [_vm._v("User with orders")]),
        _vm._v(" "),
        _c("table", { staticClass: "table table-responsive table-striped" }, [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "tbody",
            _vm._l(_vm.orders, function(order, index) {
              return _c("tr", { on: { key: index } }, [
                order.orders.length > 0
                  ? _c("td", [
                      _vm._v(
                        _vm._s(order.firstname) + " " + _vm._s(order.firstname)
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                order.orders.length > 0
                  ? _c("td", [
                      _c(
                        "button",
                        {
                          staticClass: "p-btn",
                          on: {
                            click: function($event) {
                              return _vm.editOrder(order)
                            }
                          }
                        },
                        [_vm._v("Show invoice")]
                      )
                    ])
                  : _vm._e()
              ])
            }),
            0
          )
        ])
      ]),
      _vm._v(" "),
      _vm.edit
        ? _c("div", { staticClass: "col-md-6" }, [
            _c("h2", [_vm._v("Invoice for: ")]),
            _vm._v(" "),
            _c("br"),
            _c("hr"),
            _c("br"),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                _vm._s(_vm.order.firstname) + " " + _vm._s(_vm.order.firstname)
              )
            ]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                _vm._s(_vm.order.streetaddress) +
                  ", " +
                  _vm._s(_vm.order.town) +
                  " " +
                  _vm._s(_vm.order.postcode) +
                  ", " +
                  _vm._s(_vm.order.country)
              )
            ]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "Phone: " +
                  _vm._s(_vm.order.phone) +
                  ", Email: " +
                  _vm._s(_vm.order.email)
              )
            ]),
            _vm._v(" "),
            _c("br"),
            _c("hr"),
            _c("br"),
            _vm._v(" "),
            _c("h2", [_vm._v("Items")]),
            _vm._v(" "),
            _c(
              "table",
              { staticClass: "table table-responsive table-striped" },
              [
                _vm._m(1),
                _vm._v(" "),
                _c(
                  "tbody",
                  _vm._l(_vm.order.orders, function(item) {
                    return _c("tr", [
                      _c("td", [_vm._v(_vm._s(item.product_id))]),
                      _vm._v(" "),
                      _c("td", [_vm._v("$ " + _vm._s(item.price))]),
                      _vm._v(" "),
                      _c("td", [_vm._v("$ " + _vm._s(item.discount_price))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(item.quantity))]),
                      _vm._v(" "),
                      item.discount_price
                        ? _c("td", [
                            _vm._v(
                              "$ " + _vm._s(item.discount_price * item.quantity)
                            )
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      !item.discount_price
                        ? _c("td", [
                            _vm._v("$ " + _vm._s(item.price * item.quantity))
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      item.is_delivered == 1
                        ? _c("td", [_vm._v("Yes")])
                        : _vm._e()
                    ])
                  }),
                  0
                )
              ]
            ),
            _vm._v(" "),
            _c("br"),
            _c("hr"),
            _c("br"),
            _vm._v(" "),
            _c("h2", { staticClass: "text-center" }, [
              _vm._v("Total: $ " + _vm._s(_vm.total))
            ]),
            _vm._v(" "),
            _c("br"),
            _c("hr"),
            _c("br"),
            _vm._v(" "),
            _vm.delivery > 0
              ? _c(
                  "button",
                  {
                    staticClass: "p-btn",
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        return _vm.deliver(_vm.order.id)
                      }
                    }
                  },
                  [_vm._v("Deliver")]
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.delivery == 0 ? _c("h2", [_vm._v("Delivered")]) : _vm._e()
          ])
        : _vm._e()
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("td", [_vm._v("User")]),
        _vm._v(" "),
        _c("td", [_vm._v("Action")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("td", [_vm._v("Item")]),
        _vm._v(" "),
        _c("td", [_vm._v("Price")]),
        _vm._v(" "),
        _c("td", [_vm._v("Discount Price")]),
        _vm._v(" "),
        _c("td", [_vm._v("Quantity")]),
        _vm._v(" "),
        _c("td", [_vm._v("Sum")]),
        _vm._v(" "),
        _c("td", [_vm._v("Delivered")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/ProductModal.vue?vue&type=template&id=3988972e&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/ProductModal.vue?vue&type=template&id=3988972e&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div")
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Products.vue?vue&type=template&id=897a6d62&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Products.vue?vue&type=template&id=897a6d62& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "container", attrs: { id: "rout" } },
    [
      _c("br"),
      _c("br"),
      _c("br"),
      _vm._v(" "),
      _vm.edit
        ? _c("div", { attrs: { "ml-2": "" } }, [
            _c(
              "button",
              {
                staticClass: "p-btn",
                attrs: { id: "show-btn" },
                on: { click: _vm.showModal }
              },
              [_vm._v("Izmeni Proizvod:")]
            ),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "p-btn",
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    return _vm.clearForm()
                  }
                }
              },
              [_vm._v("Povratak na prethodnu stranu")]
            )
          ])
        : _c("div", { attrs: { "ml-2": "" } }, [
            _c(
              "button",
              {
                staticClass: "p-btn",
                attrs: { id: "show-btn" },
                on: { click: _vm.showModal }
              },
              [_vm._v("Dodaj Proizvod")]
            )
          ]),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("hr"),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c(
        "b-modal",
        {
          ref: "my-modal",
          attrs: { title: "", "hide-footer": "", "hide-header": "" }
        },
        [
          _vm.edit
            ? _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.clearForm()
                    }
                  }
                },
                [_vm._v("Povratak na prethodnu stranu")]
              )
            : _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.hideModal()
                    }
                  }
                },
                [_vm._v("Povratak na prethodnu stranu")]
              ),
          _vm._v(" "),
          _c(
            "form",
            {
              attrs: { enctype: "multipart/form-data", novalidate: "" },
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  return _vm.addProduct($event)
                }
              }
            },
            [
              _c("div", { staticClass: "form-group" }, [
                _c("label", [_vm._v("Ime proizvoda: ")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.product.name,
                      expression: "product.name"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text" },
                  domProps: { value: _vm.product.name },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.product, "name", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("label", [_vm._v("Broj jedinica:")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.product.units,
                      expression: "product.units"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text" },
                  domProps: { value: _vm.product.units },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.product, "units", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("label", [_vm._v("Cena: ")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.product.price,
                      expression: "product.price"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text" },
                  domProps: { value: _vm.product.price },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.product, "price", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c("label", [_vm._v("Opis proizvoda:")]),
                _vm._v(" "),
                _c("textarea", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.product.description,
                      expression: "product.description"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { rows: "10", cols: "30", placeholder: "Content" },
                  domProps: { value: _vm.product.description },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.product, "description", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group dropbox" }, [
                _c("input", {
                  staticClass: "input-file",
                  attrs: { type: "file", accept: "image/*" },
                  on: { change: _vm.fieldChange }
                }),
                _vm._v(" "),
                _vm.image_update
                  ? _c("p", [
                      _vm._v(
                        "\n                        Image ready for upload ...\n                    "
                      )
                    ])
                  : _c("p", [
                      _vm._v(
                        "\n                        Drag your file(s) here to begin"
                      ),
                      _c("br"),
                      _vm._v(" or click to browse\n                    ")
                    ])
              ]),
              _vm._v(" "),
              _vm.edit
                ? _c(
                    "div",
                    [
                      _vm.image_update
                        ? _c(
                            "b-img",
                            _vm._b(
                              {
                                attrs: {
                                  src: _vm.image_src + _vm.product.image,
                                  fluid: "",
                                  alt: "Responsive image"
                                }
                              },
                              "b-img",
                              _vm.image_size,
                              false
                            )
                          )
                        : _c(
                            "b-img",
                            _vm._b(
                              {
                                attrs: {
                                  src: _vm.image_src + _vm.product.image,
                                  fluid: "",
                                  alt: "Responsive image"
                                }
                              },
                              "b-img",
                              _vm.image_size,
                              false
                            )
                          )
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _c("br"),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _c("b-form-select", {
                    staticClass: "mb-3",
                    attrs: {
                      options: _vm.categories,
                      "value-field": "id",
                      "text-field": "kategorija"
                    },
                    scopedSlots: _vm._u([
                      {
                        key: "first",
                        fn: function() {
                          return [
                            _vm.edit
                              ? _c(
                                  "div",
                                  _vm._l(_vm.categories, function(category) {
                                    return _c("div", [
                                      category.id === _vm.product.category_id
                                        ? _c("div", [
                                            _c(
                                              "option",
                                              {
                                                domProps: { value: category.id }
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(category.kategorija)
                                                )
                                              ]
                                            )
                                          ])
                                        : _vm._e()
                                    ])
                                  }),
                                  0
                                )
                              : _c(
                                  "option",
                                  {
                                    attrs: { disabled: "" },
                                    domProps: { value: null }
                                  },
                                  [_vm._v("-- Please select an option --")]
                                )
                          ]
                        },
                        proxy: true
                      }
                    ]),
                    model: {
                      value: _vm.product.category_id,
                      callback: function($$v) {
                        _vm.$set(_vm.product, "category_id", $$v)
                      },
                      expression: "product.category_id"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _c("label", [_vm._v("Brend:")]),
                  _vm._v(" "),
                  _c(
                    "b-form-select",
                    {
                      model: {
                        value: _vm.product.brand_id,
                        callback: function($$v) {
                          _vm.$set(_vm.product, "brand_id", $$v)
                        },
                        expression: "product.brand_id"
                      }
                    },
                    [
                      _vm.edit
                        ? _c(
                            "option",
                            { domProps: { value: _vm.product.brand_id } },
                            [_vm._v(_vm._s(_vm.product.brand_id))]
                          )
                        : _c(
                            "option",
                            {
                              attrs: { disabled: "" },
                              domProps: { value: null }
                            },
                            [_vm._v("-- Please select an option --")]
                          ),
                      _vm._v(" "),
                      _vm._l(_vm.brands, function(brand) {
                        return _c("option", { domProps: { value: brand.id } }, [
                          _vm._v(
                            "\n                            " +
                              _vm._s(brand.brand) +
                              "\n                        "
                          )
                        ])
                      })
                    ],
                    2
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _c("label", [_vm._v("Tag:")]),
                  _vm._v(" "),
                  _c(
                    "b-form-select",
                    {
                      model: {
                        value: _vm.product.tag_id,
                        callback: function($$v) {
                          _vm.$set(_vm.product, "tag_id", $$v)
                        },
                        expression: "product.tag_id"
                      }
                    },
                    [
                      _vm.edit
                        ? _c(
                            "option",
                            { domProps: { value: _vm.product.tag_id } },
                            [_vm._v(_vm._s(_vm.product.tag_id))]
                          )
                        : _c(
                            "option",
                            {
                              attrs: { disabled: "" },
                              domProps: { value: null }
                            },
                            [_vm._v("-- Please select an option --")]
                          ),
                      _vm._v(" "),
                      _vm._l(_vm.tags, function(tag) {
                        return _c("option", { domProps: { value: tag.id } }, [
                          _vm._v(
                            "\n                            " +
                              _vm._s(tag.tag) +
                              "\n                        "
                          )
                        ])
                      })
                    ],
                    2
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _c(
                    "b-form-select",
                    {
                      staticClass: "mb-3",
                      model: {
                        value: _vm.product.color,
                        callback: function($$v) {
                          _vm.$set(_vm.product, "color", $$v)
                        },
                        expression: "product.color"
                      }
                    },
                    [
                      _vm.edit
                        ? _c(
                            "option",
                            { domProps: { value: _vm.product.color } },
                            [_vm._v(_vm._s(_vm.product.size))]
                          )
                        : _c(
                            "option",
                            {
                              attrs: { disabled: "" },
                              domProps: { value: null }
                            },
                            [_vm._v("-- Please select an option --")]
                          ),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "S" } }, [_vm._v("S")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "M" } }, [_vm._v("M")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "L" } }, [_vm._v("L")]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "XS" } }, [_vm._v("XS")])
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _c(
                    "b-form-select",
                    {
                      staticClass: "mb-3",
                      model: {
                        value: _vm.product.size,
                        callback: function($$v) {
                          _vm.$set(_vm.product, "size", $$v)
                        },
                        expression: "product.size"
                      }
                    },
                    [
                      _vm.edit
                        ? _c(
                            "option",
                            { domProps: { value: _vm.product.size } },
                            [_vm._v(_vm._s(_vm.product.color))]
                          )
                        : _c(
                            "option",
                            {
                              attrs: { disabled: "" },
                              domProps: { value: null }
                            },
                            [_vm._v("-- Please select an option --")]
                          ),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "Black" } }, [
                        _vm._v("Black")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "Violet" } }, [
                        _vm._v("Violet")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "Blue" } }, [
                        _vm._v("Blue")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "Yellow" } }, [
                        _vm._v("Yellow")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "Red" } }, [
                        _vm._v("Red")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "Green" } }, [
                        _vm._v("Green")
                      ])
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "modal-footer" },
                [
                  _vm._t("footer", [
                    _vm.edit
                      ? _c("div", { staticClass: "form-group" }, [
                          _c("button", { staticClass: "p-btn" }, [
                            _vm._v("Izmeni")
                          ])
                        ])
                      : _c("div", { staticClass: "form-group" }, [
                          _c("button", { staticClass: "p-btn" }, [
                            _vm._v("Kreiraj")
                          ])
                        ])
                  ])
                ],
                2
              )
            ]
          ),
          _vm._v(" "),
          _vm.edit
            ? _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.clearForm()
                    }
                  }
                },
                [_vm._v("Povratak na prethodnu stranu")]
              )
            : _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.hideModal()
                    }
                  }
                },
                [_vm._v("Povratak na prethodnu stranu")]
              )
        ]
      ),
      _vm._v(" "),
      _c(
        "b-row",
        [
          _c(
            "b-col",
            { staticClass: "my-1", attrs: { lg: "6" } },
            [
              _c(
                "b-form-group",
                {
                  staticClass: "mb-0",
                  attrs: {
                    label: "Filter",
                    "label-cols-sm": "3",
                    "label-align-sm": "right",
                    "label-size": "sm",
                    "label-for": "filterInput"
                  }
                },
                [
                  _c(
                    "b-input-group",
                    { attrs: { size: "sm" } },
                    [
                      _c("b-form-input", {
                        attrs: {
                          type: "search",
                          id: "filterInput",
                          placeholder: "Type to Search"
                        },
                        model: {
                          value: _vm.filter,
                          callback: function($$v) {
                            _vm.filter = $$v
                          },
                          expression: "filter"
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "b-input-group-append",
                        [
                          _c(
                            "b-button",
                            {
                              attrs: { disabled: !_vm.filter },
                              on: {
                                click: function($event) {
                                  _vm.filter = ""
                                }
                              }
                            },
                            [_vm._v("Clear")]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "b-col",
            { staticClass: "my-1", attrs: { lg: "6" } },
            [
              _c(
                "b-form-group",
                {
                  staticClass: "mb-0",
                  attrs: {
                    label: "Filter On",
                    "label-cols-sm": "3",
                    "label-align-sm": "right",
                    "label-size": "sm",
                    description: "Leave all unchecked to filter on all data"
                  }
                },
                [
                  _c(
                    "b-form-checkbox-group",
                    {
                      staticClass: "mt-1",
                      model: {
                        value: _vm.filterOn,
                        callback: function($$v) {
                          _vm.filterOn = $$v
                        },
                        expression: "filterOn"
                      }
                    },
                    [
                      _c("b-form-checkbox", { attrs: { value: "units" } }, [
                        _vm._v("Units")
                      ]),
                      _vm._v(" "),
                      _c("b-form-checkbox", { attrs: { value: "price" } }, [
                        _vm._v("Price")
                      ])
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "b-col",
            { staticClass: "my-1", attrs: { sm: "5", md: "6" } },
            [
              _c(
                "b-form-group",
                {
                  staticClass: "mb-0",
                  attrs: {
                    label: "Per page",
                    "label-cols-sm": "6",
                    "label-cols-md": "4",
                    "label-cols-lg": "3",
                    "label-align-sm": "right",
                    "label-size": "sm",
                    "label-for": "perPageSelect"
                  }
                },
                [
                  _c("b-form-select", {
                    attrs: {
                      id: "perPageSelect",
                      size: "sm",
                      options: _vm.pageOptions
                    },
                    model: {
                      value: _vm.perPage,
                      callback: function($$v) {
                        _vm.perPage = $$v
                      },
                      expression: "perPage"
                    }
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("b-table", {
        attrs: {
          "show-empty": "",
          small: "",
          stacked: "md",
          striped: "",
          hover: "",
          items: _vm.products,
          fields: _vm.fields,
          "current-page": _vm.currentPage,
          "per-page": _vm.perPage,
          filter: _vm.filter,
          filterIncludedFields: _vm.filterOn,
          "sort-by": _vm.sortBy,
          "sort-desc": _vm.sortDesc,
          "sort-direction": _vm.sortDirection
        },
        on: {
          "update:sortBy": function($event) {
            _vm.sortBy = $event
          },
          "update:sort-by": function($event) {
            _vm.sortBy = $event
          },
          "update:sortDesc": function($event) {
            _vm.sortDesc = $event
          },
          "update:sort-desc": function($event) {
            _vm.sortDesc = $event
          },
          filtered: _vm.onFiltered
        },
        scopedSlots: _vm._u([
          {
            key: "cell(action)",
            fn: function(row) {
              return [
                _c("b-col", [
                  _c(
                    "button",
                    {
                      staticClass: "p-btn",
                      on: {
                        click: function($event) {
                          return _vm.editProduct(row.item)
                        }
                      }
                    },
                    [_vm._v("Edit")]
                  ),
                  _c("br"),
                  _vm._v(" "),
                  _c(
                    "button",
                    {
                      staticClass: "p-btn",
                      on: {
                        click: function($event) {
                          return _vm.deleteProduct(row.item.id)
                        }
                      }
                    },
                    [
                      _vm._v(
                        "\n                        Delete\n                    "
                      )
                    ]
                  )
                ])
              ]
            }
          }
        ])
      }),
      _vm._v(" "),
      _c(
        "b-col",
        { staticClass: "my-1", attrs: { sm: "7", md: "6" } },
        [
          _c("b-pagination", {
            staticClass: "my-0",
            attrs: {
              "total-rows": _vm.totalRows,
              "per-page": _vm.perPage,
              align: "fill",
              size: "sm"
            },
            model: {
              value: _vm.currentPage,
              callback: function($$v) {
                _vm.currentPage = $$v
              },
              expression: "currentPage"
            }
          })
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Tag.vue?vue&type=template&id=756e1402&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Tag.vue?vue&type=template&id=756e1402& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container", attrs: { id: "rout" } }, [
    _c("br"),
    _c("br"),
    _c("br"),
    _vm._v(" "),
    _c("div", [
      _vm.edit
        ? _c("div", [_c("h1", [_vm._v("Izmeni podatke o tagu:")])])
        : _c("div", [_c("h1", [_vm._v("Dodaj tag:")])]),
      _vm._v(" "),
      _c(
        "form",
        {
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.add($event)
            }
          }
        },
        [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.tag.tag,
                      expression: "tag.tag"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text" },
                  domProps: { value: _vm.tag.tag },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.tag, "tag", $event.target.value)
                    }
                  }
                })
              ])
            ])
          ]),
          _vm._v(" "),
          _c("br"),
          _vm._v(" "),
          _vm.edit
            ? _c("div", { staticClass: "form-group" }, [
                _c("button", { staticClass: "site-btn place-btn" }, [
                  _vm._v("Izmeni")
                ]),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "site-btn place-btn",
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        return _vm.clearForm()
                      }
                    }
                  },
                  [_vm._v("Povratak na prethodnu stranu")]
                )
              ])
            : _c("div", { staticClass: "form-group" }, [
                _c("button", { staticClass: "site-btn place-btn" }, [
                  _vm._v("Kreiraj")
                ])
              ])
        ]
      )
    ]),
    _vm._v(" "),
    _c("table", { staticClass: "table table-hover" }, [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "tbody",
        _vm._l(_vm.tags, function(tag) {
          return _c("tr", { key: tag.id }, [
            _c("td", [_vm._v(_vm._s(tag.id))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(tag.tag))]),
            _vm._v(" "),
            _c("td", [
              _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.editTag(tag)
                    }
                  }
                },
                [_vm._v("Edit")]
              ),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "p-btn",
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.deleteTag(tag.id)
                    }
                  }
                },
                [_vm._v("Delete")]
              )
            ])
          ])
        }),
        0
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("ID")]),
        _vm._v(" "),
        _c("th", [_vm._v("Tag")]),
        _vm._v(" "),
        _c("th", [_vm._v("Actions")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Users.vue?vue&type=template&id=fa2043a6&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Users.vue?vue&type=template&id=fa2043a6& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container", attrs: { id: "rout" } }, [
    _c("br"),
    _c("br"),
    _c("br"),
    _vm._v(" "),
    _c("table", { staticClass: "table table-responsive table-striped" }, [
      _vm._m(0),
      _vm._v(" "),
      _c(
        "tbody",
        _vm._l(_vm.users, function(user, index) {
          return _c("tr", { on: { key: index } }, [
            _c("td", [_vm._v(_vm._s(index + 1))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(user.name))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(user.email))]),
            _vm._v(" "),
            _c("td", [_vm._v(_vm._s(user.created_at))]),
            _vm._v(" "),
            _c("td", [
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _c(
                    "b-form-select",
                    {
                      staticClass: "mb-3",
                      model: {
                        value: user.is_admin,
                        callback: function($$v) {
                          _vm.$set(user, "is_admin", $$v)
                        },
                        expression: "user.is_admin"
                      }
                    },
                    [
                      _c("option", { domProps: { value: user.is_admin } }, [
                        user.is_admin == 1
                          ? _c("span", [_vm._v("Admin")])
                          : _vm._e(),
                        user.is_admin != 1
                          ? _c("span", [_vm._v("Posetilac")])
                          : _vm._e()
                      ]),
                      _vm._v(" "),
                      _c("b-form-select-option", { attrs: { value: "1" } }, [
                        _vm._v("Admin")
                      ]),
                      _vm._v(" "),
                      _c("b-form-select-option", { attrs: { value: "0" } }, [
                        _vm._v("Posetilac")
                      ])
                    ],
                    1
                  )
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("td", [
              _c(
                "button",
                {
                  staticClass: "p-btn",
                  attrs: { size: "sm" },
                  on: {
                    click: function($event) {
                      return _vm.change(user)
                    }
                  }
                },
                [_vm._v("Change")]
              )
            ])
          ])
        }),
        0
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("td"),
        _vm._v(" "),
        _c("td", [_vm._v("Name")]),
        _vm._v(" "),
        _c("td", [_vm._v("Email")]),
        _vm._v(" "),
        _c("td", [_vm._v("Joined")]),
        _vm._v(" "),
        _c("td", [_vm._v("Rola")]),
        _vm._v(" "),
        _c("td", [_vm._v("Promeni")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/Admin.vue":
/*!*******************************************!*\
  !*** ./resources/js/components/Admin.vue ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Admin_vue_vue_type_template_id_58b78718___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Admin.vue?vue&type=template&id=58b78718& */ "./resources/js/components/Admin.vue?vue&type=template&id=58b78718&");
/* harmony import */ var _Admin_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Admin.vue?vue&type=script&lang=js& */ "./resources/js/components/Admin.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Admin_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Admin.vue?vue&type=style&index=0&scope=true&lang=css& */ "./resources/js/components/Admin.vue?vue&type=style&index=0&scope=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Admin_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Admin_vue_vue_type_template_id_58b78718___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Admin_vue_vue_type_template_id_58b78718___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Admin.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Admin.vue?vue&type=script&lang=js&":
/*!********************************************************************!*\
  !*** ./resources/js/components/Admin.vue?vue&type=script&lang=js& ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Admin.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Admin.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Admin.vue?vue&type=style&index=0&scope=true&lang=css&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/Admin.vue?vue&type=style&index=0&scope=true&lang=css& ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./Admin.vue?vue&type=style&index=0&scope=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Admin.vue?vue&type=style&index=0&scope=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_style_index_0_scope_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/Admin.vue?vue&type=template&id=58b78718&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/Admin.vue?vue&type=template&id=58b78718& ***!
  \**************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_template_id_58b78718___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Admin.vue?vue&type=template&id=58b78718& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Admin.vue?vue&type=template&id=58b78718&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_template_id_58b78718___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Admin_vue_vue_type_template_id_58b78718___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/admin/Brand.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/admin/Brand.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Brand_vue_vue_type_template_id_3ebac42c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Brand.vue?vue&type=template&id=3ebac42c& */ "./resources/js/components/admin/Brand.vue?vue&type=template&id=3ebac42c&");
/* harmony import */ var _Brand_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Brand.vue?vue&type=script&lang=js& */ "./resources/js/components/admin/Brand.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Brand_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Brand_vue_vue_type_template_id_3ebac42c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Brand_vue_vue_type_template_id_3ebac42c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/admin/Brand.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/admin/Brand.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/admin/Brand.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Brand_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Brand.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Brand.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Brand_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/admin/Brand.vue?vue&type=template&id=3ebac42c&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/admin/Brand.vue?vue&type=template&id=3ebac42c& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Brand_vue_vue_type_template_id_3ebac42c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Brand.vue?vue&type=template&id=3ebac42c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Brand.vue?vue&type=template&id=3ebac42c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Brand_vue_vue_type_template_id_3ebac42c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Brand_vue_vue_type_template_id_3ebac42c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/admin/Category.vue":
/*!****************************************************!*\
  !*** ./resources/js/components/admin/Category.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Category_vue_vue_type_template_id_d1b3502e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Category.vue?vue&type=template&id=d1b3502e& */ "./resources/js/components/admin/Category.vue?vue&type=template&id=d1b3502e&");
/* harmony import */ var _Category_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Category.vue?vue&type=script&lang=js& */ "./resources/js/components/admin/Category.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Category_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Category_vue_vue_type_template_id_d1b3502e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Category_vue_vue_type_template_id_d1b3502e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/admin/Category.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/admin/Category.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/admin/Category.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Category_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Category.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Category.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Category_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/admin/Category.vue?vue&type=template&id=d1b3502e&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/admin/Category.vue?vue&type=template&id=d1b3502e& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Category_vue_vue_type_template_id_d1b3502e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Category.vue?vue&type=template&id=d1b3502e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Category.vue?vue&type=template&id=d1b3502e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Category_vue_vue_type_template_id_d1b3502e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Category_vue_vue_type_template_id_d1b3502e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/admin/Coupons.vue":
/*!***************************************************!*\
  !*** ./resources/js/components/admin/Coupons.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Coupons_vue_vue_type_template_id_4b0e4372___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Coupons.vue?vue&type=template&id=4b0e4372& */ "./resources/js/components/admin/Coupons.vue?vue&type=template&id=4b0e4372&");
/* harmony import */ var _Coupons_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Coupons.vue?vue&type=script&lang=js& */ "./resources/js/components/admin/Coupons.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Coupons_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Coupons_vue_vue_type_template_id_4b0e4372___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Coupons_vue_vue_type_template_id_4b0e4372___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/admin/Coupons.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/admin/Coupons.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/admin/Coupons.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Coupons_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Coupons.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Coupons.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Coupons_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/admin/Coupons.vue?vue&type=template&id=4b0e4372&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/admin/Coupons.vue?vue&type=template&id=4b0e4372& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Coupons_vue_vue_type_template_id_4b0e4372___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Coupons.vue?vue&type=template&id=4b0e4372& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Coupons.vue?vue&type=template&id=4b0e4372&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Coupons_vue_vue_type_template_id_4b0e4372___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Coupons_vue_vue_type_template_id_4b0e4372___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/admin/Main.vue":
/*!************************************************!*\
  !*** ./resources/js/components/admin/Main.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Main_vue_vue_type_template_id_137c6ca4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Main.vue?vue&type=template&id=137c6ca4&scoped=true& */ "./resources/js/components/admin/Main.vue?vue&type=template&id=137c6ca4&scoped=true&");
/* harmony import */ var _Main_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Main.vue?vue&type=script&lang=js& */ "./resources/js/components/admin/Main.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Main_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Main.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/admin/Main.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _Main_vue_vue_type_style_index_1_id_137c6ca4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css& */ "./resources/js/components/admin/Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");







/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__["default"])(
  _Main_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Main_vue_vue_type_template_id_137c6ca4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Main_vue_vue_type_template_id_137c6ca4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "137c6ca4",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/admin/Main.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/admin/Main.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/js/components/admin/Main.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Main.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/admin/Main.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/admin/Main.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Main.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/admin/Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/components/admin/Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css& ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_1_id_137c6ca4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=style&index=1&id=137c6ca4&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_1_id_137c6ca4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_1_id_137c6ca4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_1_id_137c6ca4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_1_id_137c6ca4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_style_index_1_id_137c6ca4_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/admin/Main.vue?vue&type=template&id=137c6ca4&scoped=true&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/admin/Main.vue?vue&type=template&id=137c6ca4&scoped=true& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_template_id_137c6ca4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Main.vue?vue&type=template&id=137c6ca4&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Main.vue?vue&type=template&id=137c6ca4&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_template_id_137c6ca4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Main_vue_vue_type_template_id_137c6ca4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/admin/Orders.vue":
/*!**************************************************!*\
  !*** ./resources/js/components/admin/Orders.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Orders_vue_vue_type_template_id_77736b10___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Orders.vue?vue&type=template&id=77736b10& */ "./resources/js/components/admin/Orders.vue?vue&type=template&id=77736b10&");
/* harmony import */ var _Orders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Orders.vue?vue&type=script&lang=js& */ "./resources/js/components/admin/Orders.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Orders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Orders_vue_vue_type_template_id_77736b10___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Orders_vue_vue_type_template_id_77736b10___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/admin/Orders.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/admin/Orders.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/components/admin/Orders.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Orders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Orders.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Orders.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Orders_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/admin/Orders.vue?vue&type=template&id=77736b10&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/admin/Orders.vue?vue&type=template&id=77736b10& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Orders_vue_vue_type_template_id_77736b10___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Orders.vue?vue&type=template&id=77736b10& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Orders.vue?vue&type=template&id=77736b10&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Orders_vue_vue_type_template_id_77736b10___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Orders_vue_vue_type_template_id_77736b10___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/admin/ProductModal.vue":
/*!********************************************************!*\
  !*** ./resources/js/components/admin/ProductModal.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProductModal_vue_vue_type_template_id_3988972e_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProductModal.vue?vue&type=template&id=3988972e&scoped=true& */ "./resources/js/components/admin/ProductModal.vue?vue&type=template&id=3988972e&scoped=true&");
/* harmony import */ var _ProductModal_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductModal.vue?vue&type=script&lang=js& */ "./resources/js/components/admin/ProductModal.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _ProductModal_vue_vue_type_style_index_0_id_3988972e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ProductModal.vue?vue&type=style&index=0&id=3988972e&scoped=true&lang=css& */ "./resources/js/components/admin/ProductModal.vue?vue&type=style&index=0&id=3988972e&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _ProductModal_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProductModal_vue_vue_type_template_id_3988972e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProductModal_vue_vue_type_template_id_3988972e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "3988972e",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/admin/ProductModal.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/admin/ProductModal.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/admin/ProductModal.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductModal_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductModal.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/ProductModal.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductModal_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/admin/ProductModal.vue?vue&type=style&index=0&id=3988972e&scoped=true&lang=css&":
/*!*****************************************************************************************************************!*\
  !*** ./resources/js/components/admin/ProductModal.vue?vue&type=style&index=0&id=3988972e&scoped=true&lang=css& ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductModal_vue_vue_type_style_index_0_id_3988972e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductModal.vue?vue&type=style&index=0&id=3988972e&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/ProductModal.vue?vue&type=style&index=0&id=3988972e&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductModal_vue_vue_type_style_index_0_id_3988972e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductModal_vue_vue_type_style_index_0_id_3988972e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductModal_vue_vue_type_style_index_0_id_3988972e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductModal_vue_vue_type_style_index_0_id_3988972e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductModal_vue_vue_type_style_index_0_id_3988972e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/admin/ProductModal.vue?vue&type=template&id=3988972e&scoped=true&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/components/admin/ProductModal.vue?vue&type=template&id=3988972e&scoped=true& ***!
  \***************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductModal_vue_vue_type_template_id_3988972e_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductModal.vue?vue&type=template&id=3988972e&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/ProductModal.vue?vue&type=template&id=3988972e&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductModal_vue_vue_type_template_id_3988972e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductModal_vue_vue_type_template_id_3988972e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/admin/Products.vue":
/*!****************************************************!*\
  !*** ./resources/js/components/admin/Products.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Products_vue_vue_type_template_id_897a6d62___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Products.vue?vue&type=template&id=897a6d62& */ "./resources/js/components/admin/Products.vue?vue&type=template&id=897a6d62&");
/* harmony import */ var _Products_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Products.vue?vue&type=script&lang=js& */ "./resources/js/components/admin/Products.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Products_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Products.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/admin/Products.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Products_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Products_vue_vue_type_template_id_897a6d62___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Products_vue_vue_type_template_id_897a6d62___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/admin/Products.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/admin/Products.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/admin/Products.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Products.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Products.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/admin/Products.vue?vue&type=style&index=0&lang=css&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/admin/Products.vue?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Products.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Products.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/admin/Products.vue?vue&type=template&id=897a6d62&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/admin/Products.vue?vue&type=template&id=897a6d62& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_template_id_897a6d62___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Products.vue?vue&type=template&id=897a6d62& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Products.vue?vue&type=template&id=897a6d62&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_template_id_897a6d62___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_template_id_897a6d62___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/admin/Tag.vue":
/*!***********************************************!*\
  !*** ./resources/js/components/admin/Tag.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Tag_vue_vue_type_template_id_756e1402___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Tag.vue?vue&type=template&id=756e1402& */ "./resources/js/components/admin/Tag.vue?vue&type=template&id=756e1402&");
/* harmony import */ var _Tag_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Tag.vue?vue&type=script&lang=js& */ "./resources/js/components/admin/Tag.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Tag_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Tag_vue_vue_type_template_id_756e1402___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Tag_vue_vue_type_template_id_756e1402___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/admin/Tag.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/admin/Tag.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/components/admin/Tag.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Tag_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Tag.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Tag.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Tag_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/admin/Tag.vue?vue&type=template&id=756e1402&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/admin/Tag.vue?vue&type=template&id=756e1402& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Tag_vue_vue_type_template_id_756e1402___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Tag.vue?vue&type=template&id=756e1402& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Tag.vue?vue&type=template&id=756e1402&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Tag_vue_vue_type_template_id_756e1402___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Tag_vue_vue_type_template_id_756e1402___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/admin/Users.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/admin/Users.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_vue_vue_type_template_id_fa2043a6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Users.vue?vue&type=template&id=fa2043a6& */ "./resources/js/components/admin/Users.vue?vue&type=template&id=fa2043a6&");
/* harmony import */ var _Users_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Users.vue?vue&type=script&lang=js& */ "./resources/js/components/admin/Users.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Users_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Users_vue_vue_type_template_id_fa2043a6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Users_vue_vue_type_template_id_fa2043a6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/admin/Users.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/admin/Users.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/admin/Users.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Users_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Users.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Users.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Users_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/admin/Users.vue?vue&type=template&id=fa2043a6&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/admin/Users.vue?vue&type=template&id=fa2043a6& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Users_vue_vue_type_template_id_fa2043a6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Users.vue?vue&type=template&id=fa2043a6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Users.vue?vue&type=template&id=fa2043a6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Users_vue_vue_type_template_id_fa2043a6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Users_vue_vue_type_template_id_fa2043a6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);