import Vue from 'vue'
 import 'es6-promise/auto';

import AOS from 'aos';
import 'aos/dist/aos.css'; 
import VueRouter from 'vue-router'
Vue.use(VueRouter)
import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios); 

import VueSweetalert2 from 'vue-sweetalert2';

Vue.use(VueSweetalert2);  

import { store } from './store/index'; 

import JwPagination from 'jw-vue-pagination';
Vue.component('jw-pagination', JwPagination);

//import Vuelidate from 'vuelidate'
//Vue.use(Vuelidate)


import Veevalidate from 'vee-validate';
Vue.use(Veevalidate, { fieldsBagName: 'veeFields' });



//Bootrsap vue
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(VueLazyload)
import App from './App.vue';
Vue.component('v-select', VueSelect.VueSelect);

const router = new VueRouter({
	mode: 'history',
	routes: [
 
		{
			path: '/login',
			name: 'login',
			// component: HomeComponent
			component: () => import('./components/Login')
		},
		{
		name: 'changepassword',
		path: '/changepassword',
		component: () => import('./components/ChangePassword.vue')
	},
		{
			path: '/register',
			name: 'register',
			// component: HomeComponent
			component: () => import('./components/Register')
		},
	 
		  {
			name: 'ecommerce',
			path: '/',
			// component: HomeComponent
			component: () => import('./components/front/Ecommerce'),
		//	 meta: {
		 //     keepAlive: true
		  //  }
		},
		{
			name: 'filter',
			path: '/filter',
			
			component: () => import('./components/front/Ecommerce.vue'),
		    props: true
		},
		 {
			name: 'contact',
			path: '/contact',
			
			component: () => import('./components/front/contact'),
		
		},
		{
			name: 'tag',
			path: '/tag/:id',
			component: () => import('./components/front/Ecommerce.vue'),
			props: true
		},
	     	{
			name: 'category',
			path: '/category/:id',
			component: () => import('./components/front/Ecommerce.vue'),
			props: true
		}, 
		
		{
			path: '/products/:id',
			name: 'single-products',
			// component: HomeComponent
			component: () => import('./components/front/SingleProduct')
			
		},

		{
			path: '/confirmation',
			name: 'confirmation',
			component: () => import('./components/front/Confirmation')

		},
		{
			path: '/check-out',
			name: 'check-out',
			component: () => import('./components/front/check-out'),
		//	props: (route) => ({ pid: route.query.pid })
		},
		{
			path: '/shopping-cart',
			name: 'shopping-cart',
			component: () => import('./components/front/shopping-cart'),
			
		},
	   
	   {
			path: '/admin/:page',
			name: 'admin-pages',
		   component: () => import('./components/Admin.vue'),    
			
			meta: {
			    requiresAuth: true,
				is_admin: true
			}
		},
		
		{
			path: '/admin',
			name: 'admin',
			component: () => import('./components/Admin.vue'),    
		   
			meta: {
				requiresAuth: true,
				is_admin: true
			}
		} 
	],
})

router.beforeEach((to, from, next) => {
	window.previousUrl = from.path
	if (to.matched.some(record => record.meta.requiresAuth)) {
		if (localStorage.getItem('shop.mdi.in.rs.jwt') == null) {
			next({
				path: '/login',
				params: { nextUrl: to.fullPath }
			})
		} else {
			let user = JSON.parse(localStorage.getItem('shop.mdi.in.rs.user'))
			if (to.matched.some(record => record.meta.is_admin)) {
				if (user.is_admin == 1) {
					next()
				}
				else {

					next({ name: 'userboard' })
				}
			}
			else if (to.matched.some(record => record.meta.is_user)) {
				if (user.is_admin == 0) {
					next()
				}
				else {
					next({ name: 'admin' })
				}
			}
			next()
		}
	} else {
		next()
	}
})

/* const app = new Vue({
	el: '#app',
	components: { App },
	router,
}); */ 
new Vue({
	el: "#app",
	created() {
		AOS.init()
	}, 
	store,
	router: router,
	render: h => h(App)
})
