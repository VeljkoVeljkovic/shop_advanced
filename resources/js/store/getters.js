export default {
	userdata: state => state.user,
	allLikes: state => state.likes,
	allCoupons: state => state.coupons,
	allCart: state => state.cart,
	cardTotal(state, getters) {
		if(getters.allCoupons!=0) {			
			return getters.allCart.reduce((total, product) => total + product.discount_price*product.quantity, 0)}
		else {return getters.allCart.reduce((total, product) => total + product.price*product.quantity, 0) }
	},
	allCategories: state => state.categories,
	
	allTags: state => state.tags,
	
	allBrands: state => state.brands,
	
	allProducts: state => state.products,
	
	callMessage: state => state.message,
	
	alreadyInCart: state => state.alreadyInCart
		
}