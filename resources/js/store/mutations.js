export default {
	addLike(state, payload) {
		let c = state.likes.findIndex(x => x.id == payload.id);
		if(c === -1) {
		state.likes.push(payload)
		state.message = 'Proizvod je dodat u listu želja'
		} else {
		state.message = 'Ovaj proizvod se već nalazi u listi vaših želja'	
		}
	},
	addCart(state, payload) {
		let c = state.cart.findIndex(x => x.id == payload.id);
		
	if(c === -1) {
		
		if(payload.quantity) {
			state.cart.push({id: payload.id, name: payload.name, price: payload.price, discount_price: 0, quantity: payload.quantity, image: payload.image})			
		} else{
		state.cart.push({id: payload.id, name: payload.name, price: payload.price, discount_price: 0, quantity: 1, image: payload.image}) }
		 state.message = 'Product added to your cart!'
		}
	else {						
		 state.message = 'Product is already in your cart!'
		}
	},
	Cart(state, payload) {
		let c = state.cart.findIndex(x => x.id == payload.id);
		let l = state.likes.findIndex(x => x.id == payload.id);
		state.likes.splice(l, 1)
			if(c === -1) {
			
				
		state.cart.push({id: payload.id, name: payload.name, price: payload.price,  discount_price: 0, quantity: 1, image: payload.image}) 
		 state.message = 'Product added to your cart!'
		}
	else {						
		 state.message = 'Product is already in your cart!'
	}
},
	deleteCart(state) {
		state.cart = []

	},
	discountCart(state, payload) {	 
	let percent = payload.c.percent/100 
	let amount = payload.c.amount
	
	  for(let i = 0; i < state.cart.length; i++){
	 	if(state.cart[i].discount_price != 0) {} else {
			if(percent>0) {
			  state.cart[i].discount_price = state.cart[i].price - state.cart[i].price*percent
			}
            if(amount>0){
		      state.cart[i].discount_price = state.cart[i].price - amount
			}
		}
	  }		   
	},
    changeQuantity(state, payload) {
		let c = state.cart.findIndex(x => x.id == payload.id);	
		state.cart[c].quantity = payload.quantity;
		state.message = 'Quantity updated!';
	},
	alreadyInCart(state, payload) {
		let c = state.cart.findIndex(x => x.id == payload.id);	
		if(c === -1) {
			state.alreadyInCart = false
		} else { state.alreadyInCart = true }
	},
	couponData(state, payload) {
		state.coupons = payload
	},
	notfilteredproducts(state) {
		state.products = state.notfilteredproducts
	},
	removeProduct(state, payload) {
		let c = state.cart.findIndex(x => x.id == payload.id);	
		state.cart.splice(c, 1);
		state.message = 'Product removed from cart!';
	},
	UserData(state, payload) {
		state.user = payload
	},
	allData: (state, data) => (
	state.categories = data.cat,
	state.tags = data.tags,
	state.brands = data.brands,
	state.products = data.product,
	state.notfilteredproducts = data.product
	),
	
    allTag: (state, data) => {
		state.products = data.product
	},
	 category: (state, data) => {
		state.products = data.product
	},
	filter: (state, data) => {
		state.products = data
	}
}