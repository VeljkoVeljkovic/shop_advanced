<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  protected $fillable = [ 'id', 'kategorija', 'p_id'];

    public function productcategory(){
            return $this->hasMany(Product::class);
        }

    public function children()
        {
        return $this->hasMany('App\Category', 'p_id', 'id');
        }
 
}
