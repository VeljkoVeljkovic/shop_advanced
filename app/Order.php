<?php

namespace App;

    use Illuminate\Database\Eloquent\Model;
   // use Illuminate\Database\Eloquent\SoftDeletes;

    class Order extends Model
    {
       // use SoftDeletes;

        protected $fillable = [
            'price', 'discount_price', 'product_id', 'user_id', 'quantity', 'is_delivered'
        ];

        public function user()
        {
            return $this->belongsTo(User::class, 'user_id');
        }

        public function product()
        {
            return $this->belongsTo(Product::class, 'product_id');
        }

    }
