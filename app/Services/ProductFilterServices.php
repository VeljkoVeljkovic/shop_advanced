<?php
namespace App\Services;
use App\Product;
use Illuminate\Http\Request;

class ProductFilterServices {
function filter($request) {

    $product = Product::orderBy('id', 'desc');
    if ($request->category)
    {
       $product->where('category_id', $request->category);
    }

    if ($request->size)
    {
       $product->where('size', $request->size);
    }
    if ($request->color)
    {
       $product->where('color', $request->color);
    }
    if ($request->min_price)
    {
       $product->where('price', '>=',  $request->min_price);
    }
     if ($request->max_price)
    {
       $product->where('price', '<=',  $request->max_price);
    }
     if ($request->brend) {

        $product->where('brand_id',  $request->brend);
          
}

   return $product = $product->get();
}
}