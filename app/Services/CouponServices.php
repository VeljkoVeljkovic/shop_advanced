<?php
namespace App\Services;
use App\Coupons;
use App\CouponsUser;


class CouponServices {
function sendingCouponToUsers($id, $users) {
  $coupon = Coupons::findOrFail($id);
  $valid_until = $coupon->valid_until;
  $newDate = date("d-m-Y", strtotime($valid_until));
  $amount = "";
  $percent = "";
  if($coupon->amount){
  $amount = $coupon->amount." rsd"; }
  if($coupon->percent){
  $percent = $coupon->percent."%";}
  $chars = "12345678987";
  $coupongenerator = "";
  $message="";

   foreach($users as $user)
  {
    $coupongenerator = "";
      for ($i = 0; $i < 10; $i++) {
    $coupongenerator .= $chars[mt_rand(0, strlen($chars)-1)];
      }
    $firstname = $user['firstname'];
    $lastname = $user['lastname'];
    $to= $user['email'];
    $subject='Kupon';
    $message="Poštovani: ".$firstname." ".$lastname." "."Možete iskoristiti kupon broj: ".$coupongenerator." "."Do: ".$newDate."."." Popust iznosi: ".$amount.$percent;
    $headers="Vaš MDI-shop";
    mail($to, $subject, $message, $headers);
    $couponUser = new CouponsUser([
      'coupons_id' => $coupon->id,
      'user_id' => $user['id'],
      'coupongenerator' => $coupongenerator
    ]);

    $couponUser->save();
  
     }
}
}