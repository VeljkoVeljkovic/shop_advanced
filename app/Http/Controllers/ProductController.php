<?php

 namespace App\Http\Controllers;

	use App\Category;
	use App\Brand;
	use App\Tag;
	use App\Product;
	use Illuminate\Http\Request;
	use App\Jobs\UnlinkImageJob;
	use App\Services\ProductServices;
	class ProductController extends Controller
	{
		public function index()
		{

			$product = Product::orderBy('id', 'desc')->get();		   
			$cat = Category::all();
			$brand = Brand::all();
			$tag = Tag::all();
			return response()->json(['product' => $product, 'cat' => $cat, 'brands' => $brand, 'tags' => $tag],200);
		}

	

		public function store(Request $request, ProductServices $productServices)
		{
		
			$imageName =   	 $productServices->upladImage($request->image);		    			  
			Product::create($productServices->format($request, $imageName));
			return response()->json(['message' => 'Proizvod je uspešno dodat!']);
		}

	
		public function update(Request $request, $id, ProductServices $productServices)
		{
			$product = Product::findOrFail($id);
	 
   // Ukoliko nije selektovana nova slika onda nije u base64 formatu i  ista je kao postojeca u tabeli
	if ($request->image == $product->image){
		$imageName = $product->image;}  else
	   {
		//  
		$imageName =   	 $productServices->upladImage($request->image);
		
		//Nakon izmene slike brise se stara slika iz foldera
	    $imageData1 = array(
			'name' => $product->image, );
		   
			dispatch(new UnlinkImageJob($imageData1));
	
		   }	
	    $product->update($productServices->format($request, $imageName));		
        return response()->json(['message' => 'Proizvod je uspešno dodat!']);
		}

		
		public function delete($id)
		{
		 $product = Product::findOrFail($id);
		 $imageData = array(
			'name' => $product->image, );		   
		dispatch(new UnlinkImageJob($imageData));		
		$product->delete();
		return response()->json(['message' => 'Proizvod je uspešno izbrisan!']);
		}

	
	}
