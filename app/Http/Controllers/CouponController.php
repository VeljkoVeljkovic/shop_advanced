<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Coupons;
use App\Services\CouponServices;
use App\User;
use App\CouponsUser;
use Illuminate\Http\Request;

class CouponController extends Controller
{
  


    public function index()
    {
         return response()->json(Coupons::all());
    }

   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $coupon = new Coupons($request->all());

        $coupon->save();

    return response()->json('successfully added');
    }

   
    public function allcustomers($id, CouponServices $couponServices)
    {
        $users = User::orderBy('id', 'desc')->get();
        $couponServices->sendingCouponToUsers($id, $users);
    		return response()->json(['users'=>$users]);
       
    }
    public function regularcustomers($id, CouponServices $couponServices) {

		$date = date("Y-m-d H:i:s", strtotime("-6 months"));
		$users = DB::table('users')->join('orders', 'users.id', '=', 'orders.user_id')->where('orders.created_at', '>', $date)->get();
    $couponServices->sendingCouponToUsers($id, $users);
		return response()->json(['users'=>$users, 'date'=>$date]);
    
	}
   
    public function update(Request $request, $id)
    {
        $coupon = Coupons::findOrFail($id);

    $coupon->update($request->all());

    return response()->json('successfully updated');
    }



	 public function check(Request $request)
    {
       $coupon = CouponsUser::with('c')->where('user_id', $request->id)->where('coupongenerator', $request->coupon)->first();
       return response()->json(['coupon'=>$coupon]);
    }


    public function delete($id)
    {
         $coupon = Coupons::findOrFail($id);
         $coupon->delete();
         $coupon->users()->detach();
         return response()->json(['message'=>'Coupon deleted'], 200);
    }
}
