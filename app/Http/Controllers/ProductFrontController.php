<?php
 namespace App\Http\Controllers;
 
	 use App\Category;
	 use App\Brand;
	 use App\Tag;
	use App\Product;
	use Illuminate\Http\Request;
    use App\Services\ProductFilterServices;
	class ProductFrontController extends Controller
	{
		public function index()
		{

			$product = Product::orderBy('id', 'desc')->get();
		   
			$cat = Category::all();
			$brand = Brand::all();
			$tag = Tag::all();
			return response()->json(['product' => $product, 'cat' => $cat, 'brands' => $brand, 'tags' => $tag],200);
		}

		public function filter(Request $request, ProductFilterServices $productFilterServices)
		{
		   $brend = $request->brend;
		   $product = $productFilterServices->filter($request);
			return response()->json(['product' => $product, 'brend' =>$brend]);
		}
		
		public function tag($id)
		{	
            	
			$product = Tag::find($id)->producttag()->orderBy('id', 'desc')->get();								
			return response()->json(['product' => $product],200);
		}

		public function category($id)
		{	
         	
			$product = Product::where('category_id', $id)->get();								
			return response()->json(['product' => $product],200);
		}
		
		}