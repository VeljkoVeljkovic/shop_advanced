<?php

 namespace App\Http\Controllers;

    use Auth;
    use App\User;
    use Validator;
    use Illuminate\Http\Request;

    class UserController extends Controller
    {
        public function index()
        {
            return response()->json(User::all());
        }

        public function login(Request $request)
        {
            $status = 401;
            $response = ['error' => 'Unauthorised'];

            if (Auth::attempt($request->only(['email', 'password']))) {
                $status = 200;
                $response = [
                    'user' => Auth::user(),
                    'token' => Auth::user()->createToken('shop.mdi.in.rs')->accessToken,
                ];
            }

            return response()->json($response, $status);
        }

		 public function changepassword(Request $request)
        {

           $user = User::where('email', $request->email)->get();
           $credentials = [
            'email' => $request['email'],
            'password' => $request['oldpassword'],
        ];

          if (Auth::attempt($credentials)) {

             $password = bcrypt($request->password);
             User::where('email', $request->email)->update(['password' => $password]);
             $status = 200;
                $response = [
                    'user' => Auth::user(),
                    'token' => Auth::user()->createToken('shop.mdi.in.rs')->accessToken,
                ];
             return response()->json($response, $status);
        }
    }
		public function changerola(Request $request) {
			$id = $request->post('id');
			$user = User::find($id);
			$user->is_admin = $request->post('rola');
			$user->save();
			/* $user->update([
            'is_admin' => $request->post('rola') ]); */

			$users = User::all();
			return response()->json(['users' => $users, 'user' => $user]);

		}

        public function register(Request $request)
        {

            $data = $request->only(['firstname','lastname', 'companyname', 'country', 'streetaddress', 'postcode', 'town', 'phone', 'email', 'password']);
            $data['password'] = bcrypt($data['password']);
            $user = User::create($data);
            return response()->json([
                'user' => $user,
                'token' => $user->createToken('shop.mdi.in.rs')->accessToken,
            ]);
        }

        public function show(User $user)
        {
            return response()->json($user);
        }



    }
